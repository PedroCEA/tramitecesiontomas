$(document).ready(function(){

$("input").focus(function(){
    $('.alert').remove();
    colorDefault('correo');
    colorDefault('n_expediente');
    colorDefault('factibilidades_slc');
    colorDefault('nombre');
    colorDefault('telefono');
    colorDefault('correoREP');
    colorDefault('superficie');
    colorDefault('totViviendas');
    colorDefault('file-1');
    colorDefault('file-2');
    colorDefault('file-3');
    colorDefault('file-4');
    colorDefault('file-5');
    colorDefault('file-6');
    colorDefault('file-7');
    colorDefault('file-8');
    colorDefault('file-9');
    colorDefault('file-10');
    colorDefault('file-11');
    colorDefault('file-12');
    colorDefault('file-1f');
    colorDefault('file-2f');
    colorDefault('file-3f');
    colorDefault('file-4f');
    colorDefault('file-5f');
    colorDefault('file-6f');
    colorDefault('file-7f');
    colorDefault('file-8f');
    colorDefault('file-9f');
    colorDefault('file-10f');
    colorDefault('file-11f');
    colorDefault('file-12f');  
});
$("select").focus(function(){
  $('.alert').remove();
  colorDefault('fideiusuario');
  colorDefault('regcons');
  colorDefault('tipviv');
  colorDefault('mun');
  colorDefault('tipRep');
  colorDefault('tipdoc');
});

    tablaCasos=$("#tblCasos").DataTable({
        // "columnDefs":[{
        //     "targets":-1,
        //     "data":null,
        //     "defaultContent":"<button type='button' class='editar btn btn-primary'><i class='fa fa-pencil'></i></button>"
        // }],
        "language": {
    "sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ningún dato disponible en esta tabla",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "Último",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                },
                "buttons": {
                    "copy": "Copiar",
                    "colvis": "Visibilidad"
                }
            }
    });

$("#btnNuevo").click(function(){
  $("#alta-caso").trigger("reset");
  //Creamos el caso
     var url = "creaCaso.php"; // El script a dónde se realizará la petición.
      var rfc = $('#rfcN').val();
      $.ajax({
        type: "POST",
        url:url,
        data: {"rfc":rfc},
        success: function(r)
            {
              console.log(r);
              $('#radNumber').val(r);
                
               
            },
    });
   $(".modal-header").css('background-color','#3393FF'); 
   $(".modal-header").css('color','white'); 
   $(".modal-title").text("Nueva Cesión de Tomas");
  
    $('#addModal').modal({ backdrop: 'static', keyboard: false });
    $('#addModal').modal('show');
});

 $('.c_expediente').change(function(){
    recargarLista();
 });


$( '.inputfile' ).each( function()
  {
    let $input   = $( this ),
      $label   = $input.next( 'label' ),
      labelVal = $label.html();

    $input.on( 'change', function( e )
    {
      let fileName = '';

      if( this.files && this.files.length > 1 ){
        fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
      }
      else if( e.target.value ){
        fileName = e.target.value.split( '\\' ).pop();
      }

      if( fileName ){
        $label.find( 'span' ).html( fileName );

      }
      else{
        $label.html( labelVal );
      }

    });// fin change
  });//fin each


$(function(){
        $("#alta-caso").on("submit", function(e){
        e.preventDefault();  
        let correo = $("#correo").val(),
            nExp = $("#n_expediente").val(),
            nfac = $("#factibilidades_slc").val(),
            radio = $('input[name=check]:checked', '#alta-caso').val(),
            nombreRepres = $("#nombre").val(),
            telRepres = $("#telefono").val(),
            coRepres = $("#correoREP").val(),
            sup = $("#superficie").val(),
            totViviendas = $("#totViviendas").val(),
            archivo1M=$("#file-1").val(),
            archivo2M=$("#file-2").val(),
            archivo3M=$("#file-3").val(),
            archivo4M=$("#file-4").val(),
            archivo5M=$("#file-5").val(),
            archivo6M=$("#file-6").val(),
            archivo7M=$("#file-7").val(),
            archivo8M=$("#file-8").val(),
            archivo9M=$("#file-9").val(),
            archivo10M=$("#file-10").val(),
            archivo11M=$("#file-11").val(),
            archivo12M=$("#file-12").val(),
            archivo1F=$("#file-1f").val(),
            archivo2F=$("#file-2f").val(),
            archivo3F=$("#file-3f").val(),
            archivo4F=$("#file-4f").val(),
            archivo5F=$("#file-5f").val(),
            archivo6F=$("#file-6f").val(),
            archivo7F=$("#file-7f").val(),
            archivo8F=$("#file-8f").val(),
            archivo9F=$("#file-9f").val(),
            archivo10F=$("#file-10f").val(),
            archivo11F=$("#file-11f").val(),
            archivo12F=$("#file-12f").val();
        
        if (correo == "" || correo == null) {
        cambiarColor("correo");
        //mostramos el mensaje de alerta
        mostarAlerta("Por favor escribe un Correo");
        return false;
        }
        if (nExp == "" || nExp == 0) {
        cambiarColor("n_expediente");
        //mostramos el mensaje de alerta
        mostarAlerta("Por favor selecciona un expediente");
        return false;
        }
        if (nfac == "" || nfac == 0) {
        cambiarColor("factibilidades_slc");
        //mostramos el mensaje de alerta
        mostarAlerta("Por favor selecciona la factibilidad a tramitar");
        return false;
        }
        if (nombreRepres == "" || nombreRepres == null) {
        cambiarColor("nombre");
        //mostramos el mensaje de alerta
        mostarAlerta("Por favor ingresa elnombre del representante");
        return false;
        }
        if (telRepres == "" || telRepres == null) {
        cambiarColor("telefono");
        //mostramos el mensaje de alerta
        mostarAlerta("Por favor ingresa el telefono del representante");
        return false;
        }
        if (coRepres == "" || coRepres == null) {
        cambiarColor("correoREP");
        //mostramos el mensaje de alerta
        mostarAlerta("Por favor ingresa el correo del representante");
        return false;
        }
        if (sup == "" || sup == null) {
        cambiarColor("superficie");
        //mostramos el mensaje de alerta
        mostarAlerta("Por favor ingresa la superficie");
        return false;
        }
        if (totViviendas == "" || totViviendas == null) {
        cambiarColor("totViviendas");
        //mostramos el mensaje de alerta
        mostarAlerta("Por favor ingresa el total de viviendas");
        return false;
        }


        if (radio == 1) {
          if (archivo1M == "" || archivo1M == null) {
              cambiarColor("file-1");
              //mostramos el mensaje de alerta
              mostarAlerta("Por favor carga Solicitud para trámite de factibilidad de servicios.");
              return false;
          }
          if (archivo2M == "" || archivo2M == null) {
              cambiarColor("file-2");
              //mostramos el mensaje de alerta
              mostarAlerta("Por favor carga Copia del informe de uso de suelo..");
              return false;
          }
          if (archivo3M == "" || archivo3M == null) {
              cambiarColor("file-3");
              //mostramos el mensaje de alerta
              mostarAlerta("Por favor carga Croquis del predio en Google Earth.");
              return false;
          }
          if (archivo4M == "" || archivo4M == null) {
              cambiarColor("file-4");
              //mostramos el mensaje de alerta
              mostarAlerta("Por favor carga Croquis del predio en plan parcial del desarrollo.");
              return false;
          }
          if (archivo5M == "" || archivo5M == null) {
              cambiarColor("file-5");
              //mostramos el mensaje de alerta
              mostarAlerta("Por favor carga Copia certificada de la escritura de propiedad del predio con datos del Registro Público de la Propiedad.");
              return false;
          }
          if (archivo6M == "" || archivo6M == null) {
              cambiarColor("file-6");
              //mostramos el mensaje de alerta
              mostarAlerta("Por favor carga Copia certificada del Acta Constitutiva con datos en el Registro Público de la Propiedad.");
              return false;
          }
          if (archivo7M == "" || archivo7M == null) {
              cambiarColor("file-7");
              //mostramos el mensaje de alerta
              mostarAlerta("Por favor carga Copia certificada del poder notarial (representante legal).");
              return false;
          }
          if (archivo8M == "" || archivo8M == null) {
              cambiarColor("file-8");
              //mostramos el mensaje de alerta
              mostarAlerta("Por favor carga Copia de la inscripción en el RFC.");
              return false;
          }
          if (archivo9M == "" || archivo9M == null) {
              cambiarColor("file-9");
              //mostramos el mensaje de alerta
              mostarAlerta("Por favor carga Copia de la identificación del representante legal.");
              return false;
          }
          if (archivo10M == "" || archivo10M == null) {
              cambiarColor("file-10");
              //mostramos el mensaje de alerta
              mostarAlerta("Por favor carga Archivo digital con el predio georreferenciado en formato KML o KMZ,sistema de coordenadas WGS84.");
              return false;
          }
          if (archivo11M == "" || archivo11M == null) {
              cambiarColor("file-11");
              //mostramos el mensaje de alerta
              mostarAlerta("Por favor carga Copia certificada de la cesión de tomas en escritura pública ante notario, especificando de que oficio inicial y cuantas tomas le ceden,así como se realizó el pago en su momento.");
              return false;
          }
          if (archivo12M == "" || archivo12M == null) {
              cambiarColor("file-12");
              //mostramos el mensaje de alerta
              mostarAlerta("Por favor carga Oficio Solicitud.");
              return false;
          }
        }else{
          if (archivo1F == "" || archivo1F == null) {
              cambiarColor("file-1f");
              //mostramos el mensaje de alerta
              mostarAlerta("Por favor carga Solicitud para trámite de factibilidad de servicios.");
              return false;
          }
          if (archivo2F == "" || archivo2F == null) {
              cambiarColor("file-2f");
              //mostramos el mensaje de alerta
              mostarAlerta("Por favor carga Copia del informe de uso de suelo.");
              return false;
          }
          if (archivo3F == "" || archivo3F == null) {
              cambiarColor("file-3f");
              //mostramos el mensaje de alerta
              mostarAlerta("Por favor carga Croquis del predio en Google Earth.");
              return false;
          }
          if (archivo4F == "" || archivo4F == null) {
              cambiarColor("file-4f");
              //mostramos el mensaje de alerta
              mostarAlerta("Por favor carga Croquis del predio en plan parcial del desarrollo.");
              return false;
          }
          if (archivo5F == "" || archivo5F == null) {
              cambiarColor("file-5f");
              //mostramos el mensaje de alerta
              mostarAlerta("Por favor carga Copia certificada de la escritura de propiedad del predio con datos del Registro Público de la Propiedad.");
              return false;
          }
          if (archivo6F == "" || archivo6F == null) {
              cambiarColor("file-6f");
              //mostramos el mensaje de alerta
              mostarAlerta("Por favor carga Copia certificada del poder notarial (representante legal).");
              return false;
          }
          if (archivo7F == "" || archivo7F == null) {
              cambiarColor("file-7f");
              //mostramos el mensaje de alerta
              mostarAlerta("Por favor carga Copia de la inscripción en el RFC.");
              return false;
          }
          if (archivo8F == "" || archivo8F == null) {
              cambiarColor("file-8f");
              //mostramos el mensaje de alerta
              mostarAlerta("Por favor carga Copia de la identificación del propietario del predio (en caso de tratarse de una persona física)");
              return false;
          }
          if (archivo9F == "" || archivo9F == null) {
              cambiarColor("file-9f");
              //mostramos el mensaje de alerta
              mostarAlerta("Por favor carga Copia de la identificación del representante legal.");
              return false;
          }
          if (archivo10F == "" || archivo10F == null) {
              cambiarColor("file-10f");
              //mostramos el mensaje de alerta
              mostarAlerta("Por favor carga Archivo digital con el predio georreferenciado en formato KML o KMZ,sistema de coordenadas WGS84.");
              return false;
          }
          if (archivo11F == "" || archivo11F == null) {
              cambiarColor("file-11f");
              //mostramos el mensaje de alerta
              mostarAlerta("Por favor carga Copia certificada de la cesión de tomas en escritura pública ante notario, especificando de que oficio inicial y cuantas tomas le ceden,así como se realizó el pago en su momento.");
              return false;
          }
          if (archivo12F == "" || archivo12F == null) {
              cambiarColor("file-12f");
              //mostramos el mensaje de alerta
              mostarAlerta("Por favor carga Oficio Solicitud.");
              return false;
          }
        }

        

        




            
        var paqueteDeDatos = new FormData();
         paqueteDeDatos.append('radNumber',$("#radNumber").prop('value'));
         paqueteDeDatos.append('rfc',$("#rfc").prop('value'));
         paqueteDeDatos.append('correo',$("#correo").prop('value'));
         paqueteDeDatos.append('n_expediente',$("#n_expediente").prop('value'));
         paqueteDeDatos.append('factibilidades_slc',$("#factibilidades_slc").prop('value'));
         paqueteDeDatos.append('numFacti',$("#numFacti").prop('value'));
         paqueteDeDatos.append('nombre',$("#nombre").prop('value'));
         paqueteDeDatos.append('telefono',$("#telefono").prop('value'));
         paqueteDeDatos.append('correoREP',$("#correoREP").prop('value'));
         paqueteDeDatos.append('superficie',$("#superficie").prop('value'));
         paqueteDeDatos.append('totViviendas',$("#totViviendas").prop('value'));
         paqueteDeDatos.append('check',$("#check").prop('value'));

         paqueteDeDatos.append('archivo1', $('#file-1')[0].files[0]);
         paqueteDeDatos.append('archivo2', $('#file-2')[0].files[0]);
         paqueteDeDatos.append('archivo3', $('#file-3')[0].files[0]);
         paqueteDeDatos.append('archivo4', $('#file-4')[0].files[0]);
         paqueteDeDatos.append('archivo5', $('#file-5')[0].files[0]);
         paqueteDeDatos.append('archivo6', $('#file-6')[0].files[0]);
         paqueteDeDatos.append('archivo7', $('#file-7')[0].files[0]);
         paqueteDeDatos.append('archivo8', $('#file-8')[0].files[0]);
         paqueteDeDatos.append('archivo9', $('#file-9')[0].files[0]);
         paqueteDeDatos.append('archivo10', $('#file-10')[0].files[0]);
         paqueteDeDatos.append('archivo11', $('#file-11')[0].files[0]);
         paqueteDeDatos.append('archivo12', $('#file-12')[0].files[0]);

         paqueteDeDatos.append('code1',$("#code1").prop('value'));
         paqueteDeDatos.append('code2',$("#code2").prop('value'));
         paqueteDeDatos.append('code3',$("#code3").prop('value'));
         paqueteDeDatos.append('code4',$("#code4").prop('value'));
         paqueteDeDatos.append('code5',$("#code5").prop('value'));
         paqueteDeDatos.append('code6',$("#code6").prop('value'));
         paqueteDeDatos.append('code7',$("#code7").prop('value'));
         paqueteDeDatos.append('code8',$("#code8").prop('value'));
         paqueteDeDatos.append('code9',$("#code9").prop('value'));
         paqueteDeDatos.append('code10',$("#code10").prop('value'));
         paqueteDeDatos.append('code11',$("#code11").prop('value'));
         paqueteDeDatos.append('code12',$("#code12").prop('value'));

         paqueteDeDatos.append('archivo1f', $('#file-1f')[0].files[0]);
         paqueteDeDatos.append('archivo2f', $('#file-2f')[0].files[0]);
         paqueteDeDatos.append('archivo3f', $('#file-3f')[0].files[0]);
         paqueteDeDatos.append('archivo4f', $('#file-4f')[0].files[0]);
         paqueteDeDatos.append('archivo5f', $('#file-5f')[0].files[0]);
         paqueteDeDatos.append('archivo6f', $('#file-6f')[0].files[0]);
         paqueteDeDatos.append('archivo7f', $('#file-7f')[0].files[0]);
         paqueteDeDatos.append('archivo8f', $('#file-8f')[0].files[0]);
         paqueteDeDatos.append('archivo9f', $('#file-9f')[0].files[0]);
         paqueteDeDatos.append('archivo10f', $('#file-10f')[0].files[0]);
         paqueteDeDatos.append('archivo11f', $('#file-11f')[0].files[0]);
         paqueteDeDatos.append('archivo12f', $('#file-12f')[0].files[0]);
            //console.log(paqueteDeDatos);

         let destino = "llamarWebService.php";
            $.ajax({
            beforeSend: function(){
            $('#precarga').html('<p>Un momento por favor ...</p>');
            },
            url: destino,
            type: 'POST', // Siempre que se envíen ficheros, por POST, no por GET.
            contentType: false,
            data: paqueteDeDatos, // Al atributo data se le asigna el objeto FormData.
            processData: false,
            cache: false, 
            success: function(resultado){ // En caso de que todo salga bien.
              //console.log(resultado);
              $('#precarga').hide(1000);
            },
            complete: function(){
                var folio = $('#radNumber').val();
                swal({
                    title: 'Buen trabajo!',
                    text: "Tu trámite ha sido enviada con el folio:"+folio,
                    type: 'success',
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'OK!'
                  }).then((result) => {
                    if (result.value) {
                      window.location.reload();
                    }
                  })


             },
            error: function (){ // Si hay algún error.
              alert("Algo ha fallado.");
            }
          });
       
           
        });
    });

});

 function obtenerForm(){
        selectRadNumber= $("#numRadNumber").val();
        selectIdCase=$("#numCaso").val();

     //alert(selectIdCase);
    $.ajax({
        type: "POST",
        url:"obtenerDatosEditar.php",
        data: {"selectRadNumber":selectRadNumber,"selectIdCase":selectIdCase},
        success: function(r)
            {
                $("#editarForm").html(r);
               
            },
    });
}


function realizaProceso(){
   
   
   var url = "llamarWebService.php"; // El script a dónde se realizará la petición.

      $.ajax({
        beforeSend: function(){
            $('#precarga').html('<p>Un momento por favor ...</p>');
        },
             type: "POST",
             url: url,
             data: $("#alta-caso").serialize(), // Adjuntar los campos del formulario enviado.
             success: function(data)
             {
                console.log(data);
                $('#precarga').hide(1000);

               //alert(data);
                //$("#respuesta").html(data); // Mostrar la respuestas del script PHP.

             },
             complete: function()
             {
                var folio = $('#radNumber').val();
                swal({
                    title: 'Buen trabajo!',
                    text: "Tu trámite ha sido enviada con el folio:"+folio,
                    type: 'success',
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'OK!'
                  }).then((result) => {
                    if (result.value) {
                      window.location.reload();
                    }
                  })


             }
                //$("#respuesta").hide(3500);



           });
           //


        return false; // Evitar ejecutar el submit del formulario.

           
}

// function realizaProcesoEditar(){
//     var url = "llamarWebServiceEditar.php"; // El script a dónde se realizará la petición.
//     var numCaso=$("#numCaso").val();
//     var n_expediente=$("#n_expediente").val();
//     var numRadNumber=$("#numRadNumber").val();
//     var num_fac=$("#num_fac").val();
//     var username=$("#edrfc").val();
//     var numfact=$("#num_fac").val();
//     var edfichero=$("#edfichero").val();
//     var code1=$("#edbase64textarea").val();
//     var edfichero2=$("#edfichero2").val();
//     var code2=$("#edbase64textarea2").val();
//     var nombreeditar=$("#nombreeditar").val();
//     var telefonoeditar=$("#telefonoeditar").val();
//     var correoREPeditar=$("#correoREPeditar").val();
//     var SeleccionadoREPeditar=$("#SeleccionadoREPeditar").val();
//     var Existeexpediente=$("#Existeexpediente").val();
//     var edreqdomestico=$("#edreqdomestico").val();
//     var edreqcomercial=$("#edreqcomercial").val();
//     var edreqindustrial=$("#edreqindustrial").val();
//     var edreqespecificar=$("#edreqespecificar").val();
//     var edreqmixto=$("#edreqmixto").val();
//     var edtotalunidades=$("#edtotalunidades").val();
// 	var rfc=$("#rfc").val();
//     //alert(nombreeditar);
//     var param={
//                  'numCaso':numCaso,
//                  'numRadNumber':numRadNumber,
//                  'n_expediente':n_expediente,
//                  'num_fac':num_fac,
//                  'username':username,
//                  'numfact':numfact,
//                  'edfichero':edfichero,
//                  'code1':code1,
//                  'edfichero2':edfichero2,
// 				 'code2':code2,
//                  'nombreeditar':nombreeditar,
//                  'telefonoeditar':telefonoeditar,
//                  'correoREPeditar':correoREPeditar,
//                  'SeleccionadoREPeditar':SeleccionadoREPeditar,
// 				 'Existeexpediente':Existeexpediente,
// 				 'edreqdomestico':edreqdomestico,
// 				 'edreqcomercial':edreqcomercial,
// 				 'edreqindustrial':edreqindustrial,
// 				 'edreqespecificar':edreqespecificar,
// 				 'edreqmixto':edreqmixto,
// 				 'edtotalunidades':edtotalunidades,
// 				 'rfc':rfc
//     }
   

//       $.ajax({
        
//         beforeSend: function(){
//             $('#precarga').html('<img id="loader" src="../img/loading.gif" />');
//         },

//              type: "POST",
//              url: url,
//              data: param,
//              dataType: "html", // Adjuntar los campos del formulario enviado.
//              success: function(data)
//              {
                
//                 $('#precarga').hide(1000);
//                 //$("#precarga").html(data); // Mostrar la respuestas del script PHP.
//                // alert(data);

//              },
//              complete: function()
//              {
//                 var folio = $('#radNumber').val();
//                 swal({
//                     title: 'Buen trabajo!',
//                     text: "Tu trámite ha sido enviada con el folio:"+folio,
//                     type: 'success',
//                     confirmButtonColor: '#3085d6',
//                     confirmButtonText: 'OK!'
//                   }).then((result) => {
//                     if (result.value) {
//                       window.location.reload();
//                     }
//                   })
              
               
//              }         
//                 //$("#respuesta").hide(3500);
           
            
             
//            });
//            //
           
//         return false; // Evitar ejecutar el submit del formulario.
           

// }

function recargarLista(){
    $.ajax({
        type: "POST",
        url: "obtenerFactibilidad.php",
        data: $("#alta-caso").serialize(),
        success:function(r){
            $('#select2lista').html(r);
        }
    });
}    

function mostrarRelacion()
{
    var url = "mostrarDetalleFactibilidad.php"; // El script a dónde se realizará la petición.
    $.ajax({
           type: "POST",
           url: url,
           data: $("#alta-caso").serialize(), // Adjuntar los campos del formulario enviado.
           success: function(data)
           {
               $("#tabla").html(data); // Mostrar la respuestas del script PHP.
           },

           complete: function()
           {
             // $("#respuesta").hide(3500);
           }
          
         });
	$.ajax({
        type: "POST",
        url: "obtenerRequerimientos.php",
        data: $("#alta-caso").serialize(),
        success:function(r){
            $('#selectrequerimientos').html(r);
        }
    });
         
    return false; // Evitar ejecutar e
}

function mostrarDoc(x){
  
  var PMoral=document.getElementById('p_moral');
  var PFisica=document.getElementById('p_fisica');
  if (x==1) {
    $("#file-1f").val("");
    $("#archivo1f").text("Seleccionar Archivo");
    $("#code1f").val("");
    $("#file-2f").val("");
    $("#archivo2f").text("Seleccionar Archivo");
    $("#code2f").val("");
    $("#file-3f").val("");
    $("#archivo3f").text("Seleccionar Archivo");
    $("#code3f").val("");
    $("#file-4f").val("");
    $("#archivo4f").text("Seleccionar Archivo");
    $("#code4f").val("");
    $("#file-5f").val("");
    $("#archivo5f").text("Seleccionar Archivo");
    $("#code5f").val("");
    $("#file-6f").val("");
    $("#archivo6f").text("Seleccionar Archivo");
    $("#code6f").val("");
    $("#file-7f").val("");
    $("#archivo7f").text("Seleccionar Archivo");
    $("#code7f").val("");
    $("#file-8f").val("");
    $("#archivo8f").text("Seleccionar Archivo");
    $("#code8f").val("");
    $("#file-9f").val("");
    $("#archivo9f").text("Seleccionar Archivo");
    $("#code9f").val("");
    $("#file-10f").val("");
    $("#archivo10f").text("Seleccionar Archivo");
    $("#code10f").val("");
    $("#file-11f").val("");
    $("#archivo11f").text("Seleccionar Archivo");
    $("#code11f").val("");
    $("#file-12f").val("");
    $("#archivo12f").text("Seleccionar Archivo");
    $("#code12f").val("");
    PMoral.classList.remove('ocultar');
    PMoral.classList.add('mostrar');  
    PFisica.classList.remove('mostrar');
    PFisica.classList.add('ocultar');
  }else{
    $("#file-1").val("");
    $("#archivo1").html('<i class="fas fa-upload"></i>').text("Seleccionar Archivo");
    $("#code1").val("");
    $("#file-2").val("");
    $("#archivo2").text("Seleccionar Archivo");
    $("#code2").val("");
    $("#file-3").val("");
    $("#archivo3").text("Seleccionar Archivo");
    $("#code3").val("");
    $("#file-4").val("");
    $("#archivo4").text("Seleccionar Archivo");
    $("#code4").val("");
    $("#file-5").val("");
    $("#archivo5").text("Seleccionar Archivo");
    $("#code5").val("");
    $("#file-6").val("");
    $("#archivo6").text("Seleccionar Archivo");
    $("#code6").val("");
    $("#file-7").val("");
    $("#archivo7").text("Seleccionar Archivo");
    $("#code7").val("");
    $("#file-8").val("");
    $("#archivo8").text("Seleccionar Archivo");
    $("#code8").val("");
    $("#file-9").val("");
    $("#archivo9").text("Seleccionar Archivo");
    $("#code9").val("");
    $("#file-10").val("");
    $("#archivo10").text("Seleccionar Archivo");
    $("#code10").val("");
    $("#file-11").val("");
    $("#archivo11").text("Seleccionar Archivo");
    $("#code11").val("");
    $("#file-12").val("");
    $("#archivo12").text("Seleccionar Archivo");
    $("#code12").val("");
    PMoral.classList.remove('mostrar');
    PMoral.classList.add('ocultar');  
    PFisica.classList.remove('ocultar');
    PFisica.classList.add('mostrar');
    
  }
  
}


function colorDefault(dato){
    $('#' + dato).css({
        border: "1px solid #999"

    });
}

//funcion para cambiar el color de borde
function cambiarColor(dato){
    $('#'+dato).css({
        borderColor:"red"
    });    
}

//funcion para mostrar la alerta
function mostarAlerta(texto){
    $("#vacio").before('<div class="alert alert-warning" role="alert">'+ texto +'</div>');
}
