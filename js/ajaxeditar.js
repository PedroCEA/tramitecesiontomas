$(document).on('click','.editar',function(){
    var edit_id=$(this).attr('id');
    $.ajax({
        url:"edit_data.php",
        type:"post",
        data:{edit_id:edit_id},
        success:function(data){
            $("#info-update").html(data);
            $(".modal-header").css('background-color','#3393FF'); 
            $(".modal-header").css('color','white'); 
            $(".modal-title").text(" Editar Cesión de Tomas");
            $('#editData').modal({ backdrop: 'static', keyboard: false });
            $("#editData").modal('show');
        }
    });

});

function realizaProcesoEditar(){
     var url = "llamarWebServiceEditar.php"; // El script a dónde se realizará la petición.
      $.ajax({
        
        beforeSend: function(){
            $('#precarga').html('<img id="loader" src="../img/loading.gif" />');
        },

             type: "POST",
             url: url,
             data: $("#edita-caso").serialize(),
             dataType: "html", // Adjuntar los campos del formulario enviado.
             success: function(data)
             {
                
                $('#precarga').hide(1000);
                //$("#precarga").html(data); // Mostrar la respuestas del script PHP.
               console.log(data);

             },
             complete: function()
             {
                var folio = $('#folioed').val();
                swal({
                    title: 'Buen trabajo!',
                    text: "Tu trámite ha sido enviada con el folio:"+folio,
                    type: 'success',
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'OK!'
                  }).then((result) => {
                    if (result.value) {
                      window.location.reload();
                    }
                  })
              
               
             }         
                //$("#respuesta").hide(3500);
           
            
             
           });
           //
           
        return false; // Evitar ejecutar el submit del formulario.
           

}