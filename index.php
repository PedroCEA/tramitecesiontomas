<?php 
error_reporting(0);
include_once 'conexion.php';
$rfc=$_GET['rfc'];

  $consulta = "SELECT [idSolicitudPadron],[Folio],WC.idCase,[Fecha],[Tipomovimiento],[Usuario],WI.idTask
  ,T.tskDisplayName
  ,S.Correcto
  FROM [SCG].[dbo].[SolicitudPadron] S
  inner join [SCG].[dbo].[WFCASE] WC
  on S.Folio = WC.radNumber
  inner join [SCG].[dbo].[WORKITEM] WI
  on wC.idCase=WI.idCase
  inner join SCG.dbo.TASK T
  on WI.idTask=T.idTask
  inner join SCG.dbo.WFUSER WF
  on S.Usuario = WF.idUser
  where WF.userName = '$rfc'
  and WI.idTask=7222
  and S.Correcto=0
  and S.Requiererevision=1
  group by [idSolicitudPadron],[Folio],WC.idCase,[Fecha],[Tipomovimiento],[Usuario],WI.idTask
  ,T.tskDisplayName
  ,S.Correcto";

  // $consulta="SELECT idWorkItem,VW.idCase as caseWI,VW.idTask AS Tarea,WC.idcase,WC.radnumber AS Folio,S.Tipomovimiento,S.Fecha,T.tskDisplayName
  // FROM [SCG].[dbo].[VWBA_WFE_WORKITEM] VW
  // INNER JOIN SCG.dbo.WFCASE WC ON WC.idcase=VW.idcase
  // inner join SCG.dbo.WFUSER US on WC.idCreatorUser=US.idUser
  // INNER JOIN SCG.dbo.SolicitudPadron S ON S.Folio = WC.radNumber
  // INNER JOIN SCG.dbo.TASK T ON VW.idTask=T.idTask
  // WHERE VW.idTask=7222 and US.userName='$rfc' and tipomovimiento=10";


  $registro = sqlsrv_query($conn,$consulta);

  $results = array();
    while( $row = sqlsrv_fetch_array( $registro)) 
    {
         $results[] = $row;
    }

?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.5">
    <title>Cesión de Tomas</title>

<!-- Bootstrap core CSS -->
<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">

    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
    <!-- Custom styles for this template -->
    <link href="css/dashboard.css" rel="stylesheet"/>
    <link rel="stylesheet" href="css/estilo.css"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.8.0/sweetalert2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
    <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" rel="stylesheet" >
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/all.css" integrity="sha384-vp86vTRFVJgpjF9jiIGPEEqYqlDwgyBgEF109VFjmqGmIY/Y4HV4d3Gp2irVfcrp" crossorigin="anonymous">
    <link rel="stylesheet" href="css/app.css">


    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
  crossorigin="anonymous"></script>	
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootstrap.bundle.min.js"></script>
    <script src="js/feather.min.js"></script>
    <script src="js/Chart.min.js"></script>


    <script src="js/ajax.js"></script>
    <script src="js/ajaxEditar.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.8.0/sweetalert2.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
    
    <!--<script src="js/custom-file-input.js"></script>-->

    </head>
    <body>
    
    
<nav class="navbar fixed-top bg-dark flex-md-nowrap p-0 shadow navcea">
  <a class="navbar-brand col-sm-3 col-md-2 mr-0"><img src="img/bizagi-logo.png" alt="" height=40px; width=185px;></a>
  <ul class="navbar-nav px-3">
    <li class="nav-item text-nowrap">
    </li>
  </ul>
</nav>

<div class="container-fluid">
  <div class="row">
    <nav class="col-md-2 d-none d-md-block bg-light sidebar">
      <div class="sidebar-sticky">
        <ul class="nav flex-column">
          <li class="nav-item">
            <a id="btnNuevo" class="btn btn-primary">
              <span data-feather="home"></span>
              <strong><i class="fas fa-plus"></i> Nueva solicitud</strong><span ></span>
            </a>
          </li>
        </ul>
      </div>
    </nav>
      
      
    <main role="main"class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="container">
        <div class="table-wrapper">
            <div class="table-title">
               <h1><strong>Bandeja de Entrada</strong></h1>
            </div>
      <div class='clearfix'></div>
      <div id="loader"></div><!-- Carga de datos ajax aqui -->
      <div id="resultados">
      <input type="hidden" id="rfcN" name="rfcN" value="<?php echo $_GET['rfc']; ?>" placeholder="">  
      <hr>
       <table id="tblCasos" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th>Numero Caso</th>
            <th>Folio</th>
            <th>Fecha</th>
            <th>Proceso</th>
            <th>Acciones</th>
        </tr>
        </thead>
        <tbody>
            <?php  foreach($results as $dat) { 
            ?> 
          <tr>
            <td><?php echo $dat['idCase']; ?></td>
            <td><?php echo $dat['Folio']; ?></td>
            <td><?php $date=$dat['Fecha']; echo date_format($date, 'Y-m-d'); ?></td>
            <td><?php echo $dat['tskDisplayName']; ?></td>
            <td><button type="button" class="editar btn btn-primary" id="<?php echo $dat['Folio']; ?>"><i class="fas fa-pencil-alt"></i></button></td>
          </tr>
        <?php  }?>
        </tbody>
    </table>  
         </div>      
        </div>
      </div>    
    </main>
  

<!-- ///////////////////////////////////////////////MODAL AGREGAR//////////////////////////////////////////////////////-->

<div class="modal" id="addModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-scrollable modal-xl">
                  <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="staticBackdropLabel">Nueva Solicitud</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                    </div>
                      <div class="modal-body">
                        <form id='alta-caso' class="form-register" name='miformulario' method='POST' enctype='multipart/form-data'>
                                    <div class="row">
                                      <div class="col-md-4">
                                        <label for="folio">Folio:</label>
                                        <input type="text" id="radNumber" name="radNumber"  class="form-control" readonly>
                                        <input type="hidden" id="rfc" name="rfc" value="<?php $rfc = $_GET['rfc']; echo $rfc; ?>">
                                      </div>
                                      <div class="col-md-8">
                                        <label for="tramite">Tramite:</label>
                                        <input type="text" id="tramite" name="tramite" class="form-control" value="Cesión de Tomas" readonly>
                                      </div>
                                    </div>
                                    <div class="row">
                                      <div class="col-md-3">
                                        <label for="fecha">Fecha:</label>
                                        <input type="date" id="fecha" class="form-control" name="fecha" value="<?php $fecha=date('Y-m-d');  echo $fecha; ?>" readonly>
                                      </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                      <div class="col-md-6">
                                        <label for="correo">Correo:</label><span class="obligado">*</span>
                                        <input type="text" id="correo" name="correo" class="form-control"  placeholder="ejemplo@sudominio.com">
                                      </div>

                                      <div class="col-md-6">
                                         <label>Regimen Fiscal:</label><span class="obligado">*</span>
                                         <br>
                                        <div class="radio">
                                          <input type="radio" name="check" id="check" value="1" onchange="mostrarDoc(1)" checked /><label for="check">Persona Moral</label>
                                        
                                         <input type="radio" name="check" id="check1" value="0" onchange="mostrarDoc(0)" /> <label for="check1">Persona Fisica</label>
                                        </div>
                                         
                                      </div>
                                    </div>
                                    <br>
                                    <h2>Colocar numero de expediente</h2>
                                    <div class="row">
                                      <div class="col-md-6">
                                       <label for="n_expediente">Nº Expediente:</label><span class="obligado">*</span>
                                       <select id='n_expediente' name='n_expediente' class='c_expediente form-control' multiple>
                                          <?php 
                                            $query="select * from A_Expediente_prod1 where rfc = '$rfc'";
                                            echo $query;
                                            $resultado=sqlsrv_query($conn,$query);  
                                            while($fila = sqlsrv_fetch_array($resultado))
                                           {
                                            ?>
                                             <option value=" <?php echo $fila['noregistro']?>"><?php echo $fila['noregistro']?></option>
                                            <?php 
                                            }
                                           ?>
                                        </select>
                                      </div>
                                      <div class="col-md-4">
                                        <label for="">No Factibilidad</label><span class="obligado">*</span>
                                        <div id="select2lista"></div>
                                      </div>
                                      <div class="col-md-2">
                                        <a tabindex="0" class="btn popover-dismiss" role="button" data-toggle="popover" data-placement="bottom" data-trigger="focus" data-content="En caso de no encontrar su factibilidad favor de comuniarse con el area de sistemas de la CEA"><img src="img/mark_help.png" alt="" srcset=""></a>
                                      </div>   
                                    </div>
                                    
                                    <div class="col-md-10">
                                        <div id='tabla'></div>
                                    </div>

                                    <div class='form-group col-md-12'>
                                        <h2>Representante Legal</h2>
                                        <h4>3.- Escriba los siguientes datos del representante legal</h4>
                                          <table class="table table-hover">
                                            <thead>
                                              <tr>
                                                <th scope="col"><label>Nombre:</label><span class="obligado">*</span></th>
                                                <th scope="col"><label>Telefono:</label><span class="obligado">*</span></th>
                                                <th scope="col"><label>Correo:</label><span class="obligado">*</span></th>
                                               
                                              </tr>
                                            </thead>
                                            <tbody>
                                              <tr>
                                                <th scope="row"><input type='text' id='nombre' name='nombre' class='form-control'/></th>
                                                <td><input type='text' id='telefono' name='telefono' class='form-control' /></td>
                                                <td>  <input type='email' id='correoREP' name='correoREP' class='form-control entrada-usuario' /></td>
                                                
                                                
                                              </tr>
                                            </tbody>
                                          </table>
                                    </div>

                                    <div class="row">
                                      <div class="col-md-6">
                                        <label for="superficie">Superficie:</label><span class="obligado">*</span>
                                        <input type="text" id="superficie" name="superficie" class="form-control">
                                      </div>
                                      <div class="col-md-6">
                                        <label for="">Cantidad total de viviendas a ceder:</label><span class="obligado">*</span>
                                        <input type="number" id="totViviendas" name="totViviendas" class="form-control">
                                      </div>
                                    </div>
                                    <br>
                                    <div class="row">

                                      <div class="col-md-12" id="p_moral">
                                        <h2>Agregar Documentos</h2>
                                        <table class="table">
                                          <thead>
                                            <tr>
                                              <th>Documento</th>
                                              <th>Anexo</th>  
                                            </tr>
                                          </thead>
                                          <tbody>
                                            <tr>
                                              <td>Solicitud para trámite de factibilidad de servicios.<span class="obligado">*</span></td>
                                              <td>
                                            <input type="file" name="file-1" id="file-1" class="form-control inputfile inputfile-7"  accept=".pdf" />
                                            <label for="file-1">
                                            <span class="iborrainputfile" id="archivo1"><i class="fas fa-upload"></i>Seleccionar archivo</span>
                                            </label>
                                              <input type='hidden' id='code1' name='code1' class='form-control' />
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>Copia del informe de uso de suelo.<span class="obligado">*</span></td>
                                              <td>
                                                <input type="file" name="file-2" id="file-2" class="form-control inputfile inputfile-7" accept=".pdf"/>
                                                <label for="file-2">
                                                <span class="iborrainputfile" id="archivo2"><i class="fas fa-upload"></i>Seleccionar archivo</span>
                                                </label>
                                              
                                              <input type=hidden id='code2' name='code2' class='form-control' />
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>Croquis del predio en Google Earth.<span class="obligado">*</span></td>
                                              <td><input type="file" name="file-3" id="file-3" class="form-control inputfile inputfile-7" accept=".pdf"/>
                                              <label for="file-3">
                                              <span class="iborrainputfile" id="archivo3"><i class="fas fa-upload"></i>Seleccionar archivo</span>
                                              </label>
                                              <input type='hidden' id='code3' name='code3' class='form-control' />
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>Croquis del predio en plan parcial del desarrollo.<span class="obligado">*</span></td>
                                              <td><input type="file" name="file-4" id="file-4" class="form-control inputfile inputfile-7" accept=".pdf"/>
                                              <label for="file-4">
                                              <span class="iborrainputfile" id="archivo4"><i class="fas fa-upload"></i>Seleccionar archivo</span>
                                              </label>
                                              <input type='hidden' id='code4' name='code4' class='form-control' />
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>Copia certificada de la escritura de propiedad del predio con datos del Registro Público de la Propiedad.<span class="obligado">*</span></td>
                                              <td><input type="file" name="file-5" id="file-5" class="form-control inputfile inputfile-7" accept=".pdf"/>
                                              <label for="file-5">
                                              <span class="iborrainputfile" id="archivo5"><i class="fas fa-upload"></i>Seleccionar archivo</span>
                                              </label>
                                              <input type='hidden' id='code5' name='code5' class='form-control' />
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>Copia certificada del Acta Constitutiva con datos en el Registro Público de la Propiedad.<span class="obligado">*</span></td>
                                              <td><input type="file" name="file-6" id="file-6" class="form-control inputfile inputfile-7" accept=".pdf"/>
                                              <label for="file-6">
                                              <span class="iborrainputfile" id="archivo6"><i class="fas fa-upload"></i>Seleccionar archivo</span>
                                              </label>
                                              <input type='hidden' id='code6' name='code6' class='form-control' />  
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>Copia certificada del poder notarial (representante legal).<span class="obligado">*</span></td>
                                              <td><input type="file" name="file-7" id="file-7" class="form-control inputfile inputfile-7" accept=".pdf"/>
                                              <label for="file-7">
                                              <span class="iborrainputfile" id="archivo7"><i class="fas fa-upload"></i>Seleccionar archivo</span>
                                              </label>
                                              <input type='hidden' id='code7' name='code7' class='form-control' />  
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>Copia de la inscripción en el RFC.<span class="obligado">*</span></td>
                                              <td><input type="file" name="file-8" id="file-8" class="form-control inputfile inputfile-7" accept=".pdf"/>
                                              <label for="file-8">
                                              <span class="iborrainputfile" id="archivo8"><i class="fas fa-upload"></i>Seleccionar archivo</span>
                                              </label>  
                                              
                                              <input type='hidden' id='code8' name='code8' class='form-control' />  
                                              </td>
                                            </tr>
                                           
                                            <tr>
                                            <td>Copia de la identificación del representante legal.<span class="obligado">*</span></td>
                                              <td><input type="file" name="file-9" id="file-9" class="form-control inputfile inputfile-7" accept=".pdf"/>
                                              <label for="file-9">
                                              <span class="iborrainputfile" id="archivo9"><i class="fas fa-upload"></i>Seleccionar archivo</span>
                                              </label>
                                              <input type='hidden' id='code9' name='code9' class='form-control' />  
                                              </td>
                                            </tr>
                                            <tr>
                                            <td>Archivo digital con el predio georreferenciado en formato KML o KMZ,sistema de coordenadas WGS84.<span class="obligado">*</span></td>
                                              <td><input type="file" name="file-10" id="file-10" class="form-control inputfile inputfile-7" accept=".pdf"/>
                                              <label for="file-10">
                                              <span class="iborrainputfile" id="archivo10"><i class="fas fa-upload"></i>Seleccionar archivo</span>
                                              </label>
                                              <input type='hidden' id='code10' name='code10' class='form-control' />  
                                              </td>
                                            </tr>
                                            <tr>
                                            <td>Copia certificada de la cesión de tomas en escritura pública ante notario, especificando de que oficio inicial y cuantas tomas le ceden,así como se realizó el pago en su momento.<span class="obligado">*</span></td>
                                              <td><input type="file" name="file-11" id="file-11" class="form-control inputfile inputfile-7" accept=".pdf"/>
                                              <label for="file-11">
                                                <span class="iborrainputfile" id="archivo11"><i class="fas fa-upload"></i>Seleccionar archivo</span>
                                                </label>
                                              <input type='hidden' id='code11' name='code11' class='form-control' />  
                                              </td>
                                            </tr>
                                             <tr>
                                            <td>Oficio Solicitud.<span class="obligado">*</span></td>
                                              <td><input type="file" name="file-12" id="file-12" class="form-control inputfile inputfile-7" accept=".pdf"/>
                                              <label for="file-12">
                                              <span class="iborrainputfile" id="archivo12"><i class="fas fa-upload"></i>Seleccionar archivo</span>
                                              </label>
                                              <input type='hidden' id='code12' name='code12' class='form-control' />  
                                              </td>
                                            </tr>

                                          </tbody>
                                        </table>
                                      </div>


                                      <div class="col-md-12 ocultar" id="p_fisica">
                                        <h2>Agregar Documentos Persona Fisica</h2>
                                        <table class="table">
                                          <thead>
                                            <tr>
                                              <th>Documento</th>
                                              <th>Anexo</th>  
                                            </tr>
                                          </thead>
                                          <tbody>
                                            <tr>
                                              <td>Solicitud para trámite de factibilidad de servicios.<span class="obligado">*</span></td>
                                              <td><input type="file" name="file-1f" id="file-1f" class="form-control inputfile inputfile-7"  />
                                              <label for="file-1f">
                                              <span class="iborrainputfile" id="archivo1f"><i class="fas fa-upload"></i>Seleccionar archivo</span>
                                              </label>
                                              <input type='hidden' id='code1f' name='code1f' class='form-control' />
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>Copia del informe de uso de suelo.<span class="obligado">*</span></td>
                                              <td><input type="file" name="file-2f" id="file-2f" class="form-control inputfile inputfile-7" />
                                              <label for="file-2f">
                                              <span class="iborrainputfile" id="archivo2f"><i class="fas fa-upload"></i>Seleccionar archivo</span>
                                              </label>
                                              <input type='hidden' id='code2f' name='code2f' class='form-control' />
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>Croquis del predio en Google Earth.<span class="obligado">*</span></td>
                                              <td><input type="file" name="file-3f" id="file-3f" class="form-control inputfile inputfile-7" />
                                              <label for="file-3f">
                                              <span class="iborrainputfile" id="archivo3f"><i class="fas fa-upload"></i>Seleccionar archivo</span>
                                              </label>
                                              <input type='hidden' id='code3f' name='code3f' class='form-control' />
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>Croquis del predio en plan parcial del desarrollo.<span class="obligado">*</span></td>
                                              <td><input type="file" name="file-4f" id="file-4f" class="form-control inputfile inputfile-7" />
                                              <label for="file-4f">
                                              <span class="iborrainputfile" id="archivo4f"><i class="fas fa-upload"></i>Seleccionar archivo</span>
                                              </label>
                                              <input type='hidden' id='code4f' name='code4f' class='form-control' />
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>Copia certificada de la escritura de propiedad del predio con datos del Registro Público de la Propiedad.<span class="obligado">*</span></td>
                                              <td><input type="file" name="file-5f" id="file-5f" class="form-control inputfile inputfile-7" />
                                              <label for="file-5f">
                                              <span class="iborrainputfile" id="archivo5f"><i class="fas fa-upload"></i>Seleccionar archivo</span>
                                              </label>
                                              <input type='hidden' id='code5f' name='code5f' class='form-control' />
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>Copia certificada del poder notarial (representante legal).<span class="obligado">*</span></td>
                                              <td><input type="file" name="file-6f" id="file-6f" class="form-control inputfile inputfile-7" />
                                              <label for="file-6f">
                                              <span class="iborrainputfile" id="archivo6f"><i class="fas fa-upload"></i>Seleccionar archivo</span>
                                              </label>
                                              <input type='hidden' id='code6f' name='code6f' class='form-control' />  
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>Copia de la inscripción en el RFC.<span class="obligado">*</span></td>
                                              <td><input type="file" name="file-7f" id="file-7f" class="form-control inputfile inputfile-7" />
                                              <label for="file-7f">
                                              <span class="iborrainputfile" id="archivo7f"><i class="fas fa-upload"></i>Seleccionar archivo</span>
                                              </label>
                                              <input type='hidden' id='code7f' name='code7f' class='form-control' />  
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>Copia de la identificación del propietario del predio (en caso de tratarse de una persona física)<span class="obligado">*</span>.</td>
                                              <td><input type="file" name="file-8f" id="file-8f" class="form-control inputfile inputfile-7" />
                                              <label for="file-8f">
                                              <span class="iborrainputfile" id="archivo8f"><i class="fas fa-upload"></i>Seleccionar archivo</span>
                                              </label>
                                              <input type='hidden' id='code8f' name='code8f' class='form-control' />  
                                              </td>
                                            </tr>
                                            <tr>
                                            <td>Copia de la identificación del representante legal.<span class="obligado">*</span></td>
                                              <td><input type="file" name="file-9f" id="file-9f" class="form-control inputfile inputfile-7" />
                                              <label for="file-9f">
                                              <span class="iborrainputfile" id="archivo9f"><i class="fas fa-upload"></i>Seleccionar archivo</span>
                                              </label>
                                              <input type='hidden' id='code9f' name='code9f' class='form-control' />  
                                              </td>
                                            </tr>
                                            <tr>
                                            <td>Archivo digital con el predio georreferenciado en formato KML o KMZ,sistema de coordenadas WGS84.<span class="obligado">*</span></td>
                                              <td><input type="file" name="file-10f" id="file-10f" class="form-control inputfile inputfile-7" />
                                              <label for="file-10f">
                                              <span class="iborrainputfile" id="archivo10f"><i class="fas fa-upload"></i>Seleccionar archivo</span>
                                              </label>
                                              <input type='hidden' id='code10f' name='code10f' class='form-control' />  
                                              </td>
                                            </tr>
                                            <tr>
                                            <td>Copia certificada de la cesión de tomas en escritura pública ante notario, especificando de que oficio inicial y cuantas tomas le ceden,así como se realizó el pago en su momento.<span class="obligado">*</span></td>
                                              <td><input type="file" name="file-11f" id="file-11f" class="form-control inputfile inputfile-7" />
                                              <label for="file-11f">
                                              <span class="iborrainputfile" id="archivo11f"><i class="fas fa-upload"></i>Seleccionar archivo</span>
                                              </label>
                                              <input type='hidden' id='code11f' name='code11f' class='form-control' />  
                                              </td>
                                            </tr>
                                             <tr>
                                            <td>Oficio Solicitud.<span class="obligado">*</span></td>
                                              <td><input type="file" name="file-12f" id="file-12f" class="form-control inputfile inputfile-7" />
                                              <label for="file-12f">
                                              <span class="iborrainputfile" id="archivo12f"><i class="fas fa-upload"></i>Seleccionar archivo</span>
                                              </label>
                                              <input type='hidden' id='code12f' name='code12f' class='form-control' />  
                                              </td>
                                            </tr>

                                          </tbody>
                                        </table>
                                      </div>

                                    </div>
                                </div>
                                 
                                <div class="modal-footer">
                                  <input type="hidden" id="vacio" name="vacio">
                                  <div id="precarga"></div>
                                  <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fas fa-times"></i> Cerrar</button> -->
                                  <button type="submit" class="btn btn-primary" ><i class="fas fa-check"></i> Enviar Solicitud</button>
                                </div>

                              </div>
                            </div>
                          </div>
                        </form>

                      </div>
                    </div>
<!--//////////////////////////////////////////////////////MODAL AGREGAR/////////////////////////////////////////////////////////////-->
<!--/////////////////////////////////////////////////////MODAL EDITAR///////////////////////////////////////////////////////////////-->
<?php
require_once("editar.php");
?> 
<!--//////////////////////////////////////////////////////MODAL EDITAR//////////////////////////////////////////////////////////////-->

  <script>
     $('.popover-dismiss').popover({
  trigger: 'focus'
})
      </script>
      
        <script>
    var handleFileSelect = function(evt) {
    var files = evt.target.files;
    var file = files[0];

    if (files && file) {
      var reader = new FileReader();

    reader.onload = function(readerEvt) {
      var binaryString = readerEvt.target.result;
      document.getElementById("code1").value = btoa(binaryString);
    };

      reader.readAsBinaryString(file);
    }
    };

    if (window.File && window.FileReader && window.FileList && window.Blob) {
      document.getElementById('file-1').addEventListener('change', handleFileSelect, false);
    } else {
      alert('The File APIs are not fully supported in this browser.');
    }
  </script>

  <script>
    var handleFileSelect = function(evt) {
    var files = evt.target.files;
    var file = files[0];

    if (files && file) {
      var reader = new FileReader();

    reader.onload = function(readerEvt) {
      var binaryString = readerEvt.target.result;
      document.getElementById("code2").value = btoa(binaryString);
    };

      reader.readAsBinaryString(file);
    }
    };

    if (window.File && window.FileReader && window.FileList && window.Blob) {
      document.getElementById('file-2').addEventListener('change', handleFileSelect, false);
    } else {
      alert('The File APIs are not fully supported in this browser.');
    }
  </script>

  <script>
    var handleFileSelect = function(evt) {
    var files = evt.target.files;
    var file = files[0];

    if (files && file) {
      var reader = new FileReader();

    reader.onload = function(readerEvt) {
      var binaryString = readerEvt.target.result;
      document.getElementById("code3").value = btoa(binaryString);
    };

      reader.readAsBinaryString(file);
    }
    };

    if (window.File && window.FileReader && window.FileList && window.Blob) {
      document.getElementById('file-3').addEventListener('change', handleFileSelect, false);
    } else {
      alert('The File APIs are not fully supported in this browser.');
    }
  </script>

  <script>
    var handleFileSelect = function(evt) {
    var files = evt.target.files;
    var file = files[0];

    if (files && file) {
      var reader = new FileReader();

    reader.onload = function(readerEvt) {
      var binaryString = readerEvt.target.result;
      document.getElementById("code4").value = btoa(binaryString);
    };

      reader.readAsBinaryString(file);
    }
    };

    if (window.File && window.FileReader && window.FileList && window.Blob) {
      document.getElementById('file-4').addEventListener('change', handleFileSelect, false);
    } else {
      alert('The File APIs are not fully supported in this browser.');
    }
  </script>

  <script>
    var handleFileSelect = function(evt) {
    var files = evt.target.files;
    var file = files[0];

    if (files && file) {
      var reader = new FileReader();

    reader.onload = function(readerEvt) {
      var binaryString = readerEvt.target.result;
      document.getElementById("code5").value = btoa(binaryString);
    };

      reader.readAsBinaryString(file);
    }
    };

    if (window.File && window.FileReader && window.FileList && window.Blob) {
      document.getElementById('file-5').addEventListener('change', handleFileSelect, false);
    } else {
      alert('The File APIs are not fully supported in this browser.');
    }
  </script>

  <script>
    var handleFileSelect = function(evt) {
    var files = evt.target.files;
    var file = files[0];

    if (files && file) {
      var reader = new FileReader();

    reader.onload = function(readerEvt) {
      var binaryString = readerEvt.target.result;
      document.getElementById("code6").value = btoa(binaryString);
    };

      reader.readAsBinaryString(file);
    }
    };

    if (window.File && window.FileReader && window.FileList && window.Blob) {
      document.getElementById('file-6').addEventListener('change', handleFileSelect, false);
    } else {
      alert('The File APIs are not fully supported in this browser.');
    }
  </script>

  <script>
    var handleFileSelect = function(evt) {
    var files = evt.target.files;
    var file = files[0];

    if (files && file) {
      var reader = new FileReader();

    reader.onload = function(readerEvt) {
      var binaryString = readerEvt.target.result;
      document.getElementById("code7").value = btoa(binaryString);
    };

      reader.readAsBinaryString(file);
    }
    };

    if (window.File && window.FileReader && window.FileList && window.Blob) {
      document.getElementById('file-7').addEventListener('change', handleFileSelect, false);
    } else {
      alert('The File APIs are not fully supported in this browser.');
    }
  </script>

  <script>
    var handleFileSelect = function(evt) {
    var files = evt.target.files;
    var file = files[0];

    if (files && file) {
      var reader = new FileReader();

    reader.onload = function(readerEvt) {
      var binaryString = readerEvt.target.result;
      document.getElementById("code8").value = btoa(binaryString);
    };

      reader.readAsBinaryString(file);
    }
    };

    if (window.File && window.FileReader && window.FileList && window.Blob) {
      document.getElementById('file-8').addEventListener('change', handleFileSelect, false);
    } else {
      alert('The File APIs are not fully supported in this browser.');
    }
  </script>

  <script>
    var handleFileSelect = function(evt) {
    var files = evt.target.files;
    var file = files[0];

    if (files && file) {
      var reader = new FileReader();

    reader.onload = function(readerEvt) {
      var binaryString = readerEvt.target.result;
      document.getElementById("code9").value = btoa(binaryString);
    };

      reader.readAsBinaryString(file);
    }
    };

    if (window.File && window.FileReader && window.FileList && window.Blob) {
      document.getElementById('file-9').addEventListener('change', handleFileSelect, false);
    } else {
      alert('The File APIs are not fully supported in this browser.');
    }
  </script>

  <script>
    var handleFileSelect = function(evt) {
    var files = evt.target.files;
    var file = files[0];

    if (files && file) {
      var reader = new FileReader();

    reader.onload = function(readerEvt) {
      var binaryString = readerEvt.target.result;
      document.getElementById("code10").value = btoa(binaryString);
    };

      reader.readAsBinaryString(file);
    }
    };

    if (window.File && window.FileReader && window.FileList && window.Blob) {
      document.getElementById('file-10').addEventListener('change', handleFileSelect, false);
    } else {
      alert('The File APIs are not fully supported in this browser.');
    }
  </script>

  <script>
    var handleFileSelect = function(evt) {
    var files = evt.target.files;
    var file = files[0];

    if (files && file) {
      var reader = new FileReader();

    reader.onload = function(readerEvt) {
      var binaryString = readerEvt.target.result;
      document.getElementById("code11").value = btoa(binaryString);
    };

      reader.readAsBinaryString(file);
    }
    };

    if (window.File && window.FileReader && window.FileList && window.Blob) {
      document.getElementById('file-11').addEventListener('change', handleFileSelect, false);
    } else {
      alert('The File APIs are not fully supported in this browser.');
    }
  </script>

  <script>
    var handleFileSelect = function(evt) {
    var files = evt.target.files;
    var file = files[0];

    if (files && file) {
      var reader = new FileReader();

    reader.onload = function(readerEvt) {
      var binaryString = readerEvt.target.result;
      document.getElementById("code12").value = btoa(binaryString);
    };

      reader.readAsBinaryString(file);
    }
    };

    if (window.File && window.FileReader && window.FileList && window.Blob) {
      document.getElementById('file-12').addEventListener('change', handleFileSelect, false);
    } else {
      alert('The File APIs are not fully supported in this browser.');
    }
  </script>

 <!--persona fisica-->
      </script>
      
        <script>
    var handleFileSelect = function(evt) {
    var files = evt.target.files;
    var file = files[0];

    if (files && file) {
      var reader = new FileReader();

    reader.onload = function(readerEvt) {
      var binaryString = readerEvt.target.result;
      document.getElementById("code1f").value = btoa(binaryString);
    };

      reader.readAsBinaryString(file);
    }
    };

    if (window.File && window.FileReader && window.FileList && window.Blob) {
      document.getElementById('file-1f').addEventListener('change', handleFileSelect, false);
    } else {
      alert('The File APIs are not fully supported in this browser.');
    }
  </script>

  <script>
    var handleFileSelect = function(evt) {
    var files = evt.target.files;
    var file = files[0];

    if (files && file) {
      var reader = new FileReader();

    reader.onload = function(readerEvt) {
      var binaryString = readerEvt.target.result;
      document.getElementById("code2f").value = btoa(binaryString);
    };

      reader.readAsBinaryString(file);
    }
    };

    if (window.File && window.FileReader && window.FileList && window.Blob) {
      document.getElementById('file-2f').addEventListener('change', handleFileSelect, false);
    } else {
      alert('The File APIs are not fully supported in this browser.');
    }
  </script>

  <script>
    var handleFileSelect = function(evt) {
    var files = evt.target.files;
    var file = files[0];

    if (files && file) {
      var reader = new FileReader();

    reader.onload = function(readerEvt) {
      var binaryString = readerEvt.target.result;
      document.getElementById("code3f").value = btoa(binaryString);
    };

      reader.readAsBinaryString(file);
    }
    };

    if (window.File && window.FileReader && window.FileList && window.Blob) {
      document.getElementById('file-3f').addEventListener('change', handleFileSelect, false);
    } else {
      alert('The File APIs are not fully supported in this browser.');
    }
  </script>

  <script>
    var handleFileSelect = function(evt) {
    var files = evt.target.files;
    var file = files[0];

    if (files && file) {
      var reader = new FileReader();

    reader.onload = function(readerEvt) {
      var binaryString = readerEvt.target.result;
      document.getElementById("code4f").value = btoa(binaryString);
    };

      reader.readAsBinaryString(file);
    }
    };

    if (window.File && window.FileReader && window.FileList && window.Blob) {
      document.getElementById('file-4f').addEventListener('change', handleFileSelect, false);
    } else {
      alert('The File APIs are not fully supported in this browser.');
    }
  </script>

  <script>
    var handleFileSelect = function(evt) {
    var files = evt.target.files;
    var file = files[0];

    if (files && file) {
      var reader = new FileReader();

    reader.onload = function(readerEvt) {
      var binaryString = readerEvt.target.result;
      document.getElementById("code5f").value = btoa(binaryString);
    };

      reader.readAsBinaryString(file);
    }
    };

    if (window.File && window.FileReader && window.FileList && window.Blob) {
      document.getElementById('file-5f').addEventListener('change', handleFileSelect, false);
    } else {
      alert('The File APIs are not fully supported in this browser.');
    }
  </script>

  <script>
    var handleFileSelect = function(evt) {
    var files = evt.target.files;
    var file = files[0];

    if (files && file) {
      var reader = new FileReader();

    reader.onload = function(readerEvt) {
      var binaryString = readerEvt.target.result;
      document.getElementById("code6f").value = btoa(binaryString);
    };

      reader.readAsBinaryString(file);
    }
    };

    if (window.File && window.FileReader && window.FileList && window.Blob) {
      document.getElementById('file-6f').addEventListener('change', handleFileSelect, false);
    } else {
      alert('The File APIs are not fully supported in this browser.');
    }
  </script>

  <script>
    var handleFileSelect = function(evt) {
    var files = evt.target.files;
    var file = files[0];

    if (files && file) {
      var reader = new FileReader();

    reader.onload = function(readerEvt) {
      var binaryString = readerEvt.target.result;
      document.getElementById("code7f").value = btoa(binaryString);
    };

      reader.readAsBinaryString(file);
    }
    };

    if (window.File && window.FileReader && window.FileList && window.Blob) {
      document.getElementById('file-7f').addEventListener('change', handleFileSelect, false);
    } else {
      alert('The File APIs are not fully supported in this browser.');
    }
  </script>

  <script>
    var handleFileSelect = function(evt) {
    var files = evt.target.files;
    var file = files[0];

    if (files && file) {
      var reader = new FileReader();

    reader.onload = function(readerEvt) {
      var binaryString = readerEvt.target.result;
      document.getElementById("code8f").value = btoa(binaryString);
    };

      reader.readAsBinaryString(file);
    }
    };

    if (window.File && window.FileReader && window.FileList && window.Blob) {
      document.getElementById('file-8f').addEventListener('change', handleFileSelect, false);
    } else {
      alert('The File APIs are not fully supported in this browser.');
    }
  </script>

  <script>
    var handleFileSelect = function(evt) {
    var files = evt.target.files;
    var file = files[0];

    if (files && file) {
      var reader = new FileReader();

    reader.onload = function(readerEvt) {
      var binaryString = readerEvt.target.result;
      document.getElementById("code9f").value = btoa(binaryString);
    };

      reader.readAsBinaryString(file);
    }
    };

    if (window.File && window.FileReader && window.FileList && window.Blob) {
      document.getElementById('file-9f').addEventListener('change', handleFileSelect, false);
    } else {
      alert('The File APIs are not fully supported in this browser.');
    }
  </script>

  <script>
    var handleFileSelect = function(evt) {
    var files = evt.target.files;
    var file = files[0];

    if (files && file) {
      var reader = new FileReader();

    reader.onload = function(readerEvt) {
      var binaryString = readerEvt.target.result;
      document.getElementById("code10f").value = btoa(binaryString);
    };

      reader.readAsBinaryString(file);
    }
    };

    if (window.File && window.FileReader && window.FileList && window.Blob) {
      document.getElementById('file-10f').addEventListener('change', handleFileSelect, false);
    } else {
      alert('The File APIs are not fully supported in this browser.');
    }
  </script>

  <script>
    var handleFileSelect = function(evt) {
    var files = evt.target.files;
    var file = files[0];

    if (files && file) {
      var reader = new FileReader();

    reader.onload = function(readerEvt) {
      var binaryString = readerEvt.target.result;
      document.getElementById("code11f").value = btoa(binaryString);
    };

      reader.readAsBinaryString(file);
    }
    };

    if (window.File && window.FileReader && window.FileList && window.Blob) {
      document.getElementById('file-11f').addEventListener('change', handleFileSelect, false);
    } else {
      alert('The File APIs are not fully supported in this browser.');
    }
  </script>

  <script>
    var handleFileSelect = function(evt) {
    var files = evt.target.files;
    var file = files[0];

    if (files && file) {
      var reader = new FileReader();

    reader.onload = function(readerEvt) {
      var binaryString = readerEvt.target.result;
      document.getElementById("code12f").value = btoa(binaryString);
    };

      reader.readAsBinaryString(file);
    }
    };

    if (window.File && window.FileReader && window.FileList && window.Blob) {
      document.getElementById('file-12f').addEventListener('change', handleFileSelect, false);
    } else {
      alert('The File APIs are not fully supported in this browser.');
    }
  </script>
    <!--////////////////////////////////////// scripts para base 64 modal agregar ///////////////////////////////////////////////////////-->

</body>
</html>        