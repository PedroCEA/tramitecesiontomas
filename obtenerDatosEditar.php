<?php 

include_once 'conexion.php';

$caso=$_POST['selectIdCase'];
$folio=$_POST['selectRadNumber'];
  
  $consulta= "select top 1 ex.Noexpediente, sp.Folio, sp.Fecha,sp.Correo,af.No_Factibilidad,c.Cambioprorroga,r.Nombre,r.Telefono,r.Correo
  from Expediente ex 
  left join SolicitudPadron sp 
  on ex.SolicitudPadron=sp.idSolicitudPadron 
  left join A_Factibilidades af 
  on af.No_Expediente=ex.Noexpediente 
  left join Cedula c
  on c.idCedula=ex.Cedula
  left join Representantes r                                                                                             
  on r.Expediente=ex.idExpediente
  where sp.Folio ='$folio'  and r.Nombre is not null
  group by ex.Noexpediente,sp.Folio, sp.Fecha,sp.Correo,af.No_Factibilidad,c.Cambioprorroga,r.Nombre,r.Telefono,r.Correo order by No_Factibilidad desc"; 

  $sql=sqlsrv_query($conn,$consulta);
  $values = sqlsrv_fetch_array($sql);


  $fechae=$values['Fecha'];
  $correoe=$values['Correo'];
  $noExpediente=$values['Noexpediente'];
  $numFactibili=$values['No_Factibilidad'];
  $motivoProrroga=$values['Cambioprorroga'];
  $nombreRe=$values['Nombre'];
  $telefenoRe=$values['Telefono'];
  $correoRe=$values[8];

$sqlObs=" select ob.Observacion from Expediente ex 
  inner join SolicitudPadron sp on sp.idSolicitudPadron = ex.SolicitudPadron
  inner join Cedula c on c.idCedula = ex.Cedula 
  inner join Observaciones ob on ob.Expediente = ex.idExpediente
  where ex.Noexpediente = '$noExpediente' and sp.Folio = '$folio'";
  
  $sql1=sqlsrv_query($conn,$sqlObs);
  $result = sqlsrv_fetch_array($sql1);
 

  $obseva=$result['Observacion'];

$form ="<h4>1.- Coloca el correo </h4>";
$form.="<form id='edita-caso' name='miformulario' method='POST' enctype='multipart/form-data'>";
$form.="<div class='form-group row'>";
$form.="<div class='form-group col-md-4'>";
$form.="<label>Folio:</label>";
$form.="<input type='text' id='numCaso' name='numCaso' class='form-control' readonly='readonly' value='$caso'/>";
$form.="<input type='text' id='numRadNumber' name='numRadNumber' class='form-control' readonly='readonly' value='$folio'/>"; 

$form.="</div>";
$form.="<div class='form-group col-md-6'>";
$form.="<label>Opción de Trámite:</label>";
$form.="<input type='text' id='proceso' name='proceso' value='Otorgamiento de prórroga de vigencia' class='form-control' readonly='readonly'>";
$form.="</div>";
$form.="<div class='form-group col-md-4'>";
$form.="<label>Fecha:</label>";
$form.="<input type='text' id='editFecha' name='editFecha' class='form-control' readonly='readonly'>";
$form.="</div>";
$form.="<div class='form-group col-md-6'>";
$form.="<label>Observaciones</label>";
$form.="<textarea name='observa' class='form-control' readonly='readonly'>$obseva</textarea>";
$form.="</div>";
$form.="<div class='form-group col-md-10'>";
$form.="<label>Correo electronico para notificar:</label>";
$form.="<input type='email' id='inEmail' name='correo' class='form-control' required='required' value='$correoe' onkeyup='add();'readonly='readonly'>";
$form.="</br>";
$form.="<h4>2.- Introduce el numero de expediente</h4>";
$form.="</div>";
$form.="<div class='form-group col-md-5'>";
$form.="<label>No_Expediente</label>";
$form.="<div class='input-group mb-5'>";
$form.="<input type='text' id='n_expediente' name='n_expediente' class='form-control' value='$noExpediente' aria-label='# de Expediente' aria-describedby='button-addon2' readonly='readonly'>";
$form.="<div class='input-group-append'>";
$form.="<button class='btn btn-outline-secondary' type='button' id='button-addon2' onclick=''>Buscar</button>";
$form.="</div>";
$form.="</div>";
$form.="</div>";
$form.="<div class='form-group col-md-4'>";
$form.="<label>Seleccionar factibilidad:</label>";
$form.="<div id='select2lista'><input type='text' id='num_fac' name='num_fac' value='$numFactibili' class='form-control' readonly='readonly'></div>";
$form.="<div id='select3lista'></div>";
$form.="</div>";
$form.="<div class='form-group col-md-2'>";
$form.="<a tabindex='0' class='btn popover-dismiss' role='button' data-toggle='popover' data-placement='bottom' data-trigger='focus' data-content='En caso de no encontrar su factibilidad favor de comuniarse con el area de sistemas de la CEA'><img src='img/mark_help.png' alt='' srcset=''></a>";
$form.="</div>";

$form.="<div class='form-group col-md-10'>";
$form.="<h2>Representante Legal</h2>";
$form.="<h4>4.- Escriba los siguientes datos del representante legal</h4>";
$form.="<table class='table table-hover'>";
$form.="<thead>";
$form.="<tr>";
$form.="<th scope='col'><label>Nombre:</label></th>";
$form.="<th scope='col'><label>Telefono:</label></th>";
$form.="<th scope='col'><label>Correo:</label></th>";
$form.="</tr>";
$form.="</thead>";
$form.="<tbody>";
$form.="<tr>";
$form.="<th scope='row'><input type='text' id='ednombre' name='ednombre' class='form-control' value='$nombreRe'/></th>";
$form.="<td><input type='text' id='edtelefono' name='edtelefono' class='form-control' value='$telefenoRe' /></td>";
$form.="<td><input type='email' id='edcorreoREP' name='edcorreoREP' class='form-control entrada-usuario' value='$correoRe' /></td>";
$form.="</tr>";
$form.="</tbody>";
$form.="</table>";
$form.="</div>";
$form.="<div class='form-group col-md-10'>";
$form.="<label><h4>5.- Describa el motivo de la prórroga e indique el folio de factibilidad vigente:</h4></label>";
$form.="<textarea id='textopro' name='textopro' class='form-control' >$motivoProrroga</textarea>"; 
$form.="</div>";
$form.="</div>";          

$form.="</div>";


printf($form);


?>
