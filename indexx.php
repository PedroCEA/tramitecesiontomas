<?php 
error_reporting(0);
include_once 'conexion.php';
$rfc=$_GET['rfc'];

$consulta = "SELECT [idSolicitudPadron],[Folio],WC.idCase,[Fecha],[Tipomovimiento],[Usuario],WI.idTask
  ,T.tskDisplayName
  ,S.Correcto
  FROM [SCG].[dbo].[SolicitudPadron] S
  inner join [SCG].[dbo].[WFCASE] WC
  on S.Folio = WC.radNumber
  inner join [SCG].[dbo].[WORKITEM] WI
  on wC.idCase=WI.idCase
  inner join SCG.dbo.TASK T
  on WI.idTask=T.idTask
  inner join SCG.dbo.WFUSER WF
  on S.Usuario = WF.idUser
  where WF.userName = '$rfc'
  and WI.idTask=7147
  and S.Correcto=0";


  $registro = sqlsrv_query($conn,$consulta);

  $results = array();
    while( $row = sqlsrv_fetch_array( $registro, SQLSRV_FETCH_ASSOC) ) 
    {
         $results[] = $row;
    }

?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.5">
    <title>Otorgamiento Prórroga</title>

<!-- Bootstrap core CSS -->
<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">

    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
    <!-- Custom styles for this template -->
    <link href="css/dashboard.css" rel="stylesheet"/>
    <link rel="stylesheet" href="css/estilo.css"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.8.0/sweetalert2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
    <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" rel="stylesheet" >

    <script src="https://code.jquery.com/jquery-3.4.1.js"></script>	
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootstrap.bundle.min.js"></script>
    <script src="js/feather.min.js"></script>
    <script src="js/Chart.min.js"></script>
    
    <script src="js/ajax.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.8.0/sweetalert2.min.js"></script>
    <script type="module" src="https://unpkg.com/ionicons@4.5.10-0/dist/ionicons/ionicons.esm.js"></script>
    <script nomodule="" src="https://unpkg.com/ionicons@4.5.10-0/dist/ionicons/ionicons.js"></script> 
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
      </head>
      <body>
    
    
<nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
  <a class="navbar-brand col-sm-3 col-md-2 mr-0"><img src="img/bizagi-logo.png" alt="" height=30px; width=155px;></a>
  <ul class="navbar-nav px-3">
    <li class="nav-item text-nowrap">
    </li>
  </ul>
</nav>

<div class="container-fluid">
  <div class="row">
    <nav class="col-md-2 d-none d-md-block bg-light sidebar">
      <div class="sidebar-sticky">
        <ul class="nav flex-column">
          <li class="nav-item">
            <a id="btnNuevo" class="btn btn-success">
              <span data-feather="home"></span>
              <strong><ion-icon name="add-circle-outline"></ion-icon>Nueva solicitud</strong><span ></span>
            </a>
          </li>
        </ul>
      </div>
    </nav>
      
      
    <main role="main"class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="container">
        <div class="table-wrapper">
            <div class="table-title">
               <h1><strong>Bandeja de Entrada</strong></h1>
            </div>
      <div class='clearfix'></div>
      <div id="loader"></div><!-- Carga de datos ajax aqui -->
      <div id="resultados">
      <hr>
       <table id="tblCasos" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th>Numero Caso</th>
            <th>Folio</th>
            <th>Fecha</th>
            <th>Proceso</th>
            <th>Acciones</th>
        </tr>
        </thead>
        <tbody>
            <?php  foreach($results as $dat) { 
            ?> 
          <tr>
            <td><?php echo $dat['idCase']; ?></td>
            <td><?php echo $dat['Folio']; ?></td>
            <td><?php $date=$dat['Fecha']; echo date_format($date, 'Y-m-d'); ?></td>
            <td><?php echo $dat['tskDisplayName']; ?></td>
            <td></td>
          </tr>
        <?php  }?>
        </tbody>
    </table>  
         </div>      
        </div>
      </div>    
    </main>
  
<!--modal agregar -->

<!-- Modal -->
<?php require_once 'obtenerNuevoCaso.php'; ?>
<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="titulo"><strong></strong></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div><!-- cierra el header -->
      <div class="modal-body">
        <h4>1.- Coloca el correo </h4>
        <form id='alta-caso' name='miformulario' method='POST' enctype='multipart/form-data'>
      <div class='form-group row'>
            <div class='form-group col-md-4'>
                <label>Folio:</label>
          <input type='text' id='radNumber' name='radNumber' class='form-control' value='' readonly='readonly' /> 
          <input type="hidden" id="rfc" name="rfc" value="<?php $rfc = $_GET['rfc']; echo $rfc; ?>">
            </div>
            <div class='form-group col-md-6'>
                <label>Opción de Trámite:</label>
                <input type='text' id='intramite' name='tramite' value='Otorgamiento de prórroga de vigencia' class='form-control' readonly='readonly' >
            </div>
            <div class='form-group col-md-4'>
           <label>Fecha:</label>
           <div><?php echo fechaSer()?></div>
           </div>
            <div class='form-group col-md-10'>
                <label>Correo electronico para notificar:</label>
                <input type='email' id='inEmail' name='correo' class='form-control' required='required' placeholder='Ejemplo@sudominio.com' onkeyup='add();'>
                </br>
                <h4>2.- Introduce el numero de expediente</h4>
            </div>
        <div class='form-group col-md-5'>
        <label>No_Expediente</label>
        <div class='input-group mb-5'>
        <input type='text' id='n_expediente' name='n_expediente' class='form-control' aria-label='# de Expediente' aria-describedby='button-addon2'/>
        <div class='input-group-append'>
        <button class='btn btn-outline-secondary' type='button' id='button-addon2' onclick='recargarLista()'>Buscar</button>
        </div>
        </div>
        </div>
        <div class='form-group col-md-4'>
        <label>Seleccionar factibilidad:</label>
        <div id='select2lista'></div>
        <div id='select3lista'></div>
        </div>
        <div class='form-group col-md-2'>
        <a tabindex="0" class="btn popover-dismiss" role="button" data-toggle="popover" data-placement="bottom" data-trigger="focus" data-content="En caso de no encontrar su factibilidad favor de comuniarse con el area de sistemas de la CEA"><img src="img/mark_help.png" alt="" srcset=""></a>
        </div>

        <div class='form-group col-md-10'>
        <label>Detalle Factibilidad:</label>
        <div id='tabla'></div>
        </div>
        
        
        <div class='form-group col-md-10'>
        <h4>3.- Seleccione los siguientes archivos desde su computadora.</h4>
        <table class="table table-hover">
  <thead>
    <tr>
      <th scope="col">Documento</th>
      <th scope="col">Anexo</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row"><label>FORMATO DE SOLICITUD DEL DESARROLLADOR</label></th>
      <td><input type="file" id="filePicker" name="archivo" class="form-control" onchange="fichero.value=this.value.replace('C:\\fakepath\\', '');">
        <input type="hidden" name="fichero" class="form-control">
        <br>
        <input type='hidden' id='base64textarea' name='code1' class='form-control' /></td>
      </tr>
    <tr>
      <th scope="row"><label>Copia de factibilidad autorizada y vigente</label></th>
      <td><input type="file" id="filePicker2" name="archivo2" class="form-control" onchange="fichero2.value=this.value.replace('C:\\fakepath\\', '');">
        <input type="hidden" name="fichero2" class="form-control">
        <br>
        <input type='hidden' id='base64textarea2' name='code2' class='form-control' value=''/></td>
    </tr>
    <tr>
      <th scope="row"><label>Archivo polígono KML (para todos los tramites ingresados antes de enero 2018)</label></th>
      <td colspan="2"><input type="file" id="filePicker3" name="archivo3" class="form-control" onchange="fichero3.value=this.value.replace('C:\\fakepath\\', '');">
        <input type="hidden" name="fichero3" class="form-control">
        <br>
        <input type='hidden' id='base64textarea3' name='code3' class='form-control' /></td>

    </tr>
  </tbody>
</table>   
</div>
        <div class='form-group col-md-10'>
        <h2>Representante Legal</h2>
        <h4>4.- Escriba los siguientes datos del representante legal</h4>
    <table class="table table-hover">
  <thead>
    <tr>
      <th scope="col"><label>Nombre:</label></th>
      <th scope="col"><label>Telefono:</label></th>
      <th scope="col"><label>Correo:</label></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row"><input type='text' id='nombre' name='nombre' class='form-control' required /></th>
      <td><input type='text' id='telefono' name='telefono' class='form-control' required /></td>
      <td>  <input type='email' id='correoREP' name='correoREP' class='form-control entrada-usuario' required /></td>
    </tr>
  </tbody>
</table>
          </div>
          <div class='form-group col-md-10'>
          <label><h4>5.- Describa el motivo de la prórroga e indique el folio de factibilidad vigente:</h4></label>
          <textarea id='text' name='texto' class='form-control' required></textarea> 
          </div>
       


      
      <div class="modal-footer">
       <div id="precarga"></div>
      <input type='button' id='btn_enviar' name='creaCaso' value='Enviar Solicitud' class='btn btn-primary' onclick='realizaProceso()'/>
      <!--<input type='button' id='guardar' name='guarda_btn' value='GrabarDatos' class='btn btn-secondary' onclick='realizaProcesoGuardar()'/>-->
      <input type='hidden' id='transaccion'name='transaccion' value='insertar'>
      </div>
    </div>
  </div>
</div>
<!--modal agregar-->

<!--//////////////////////////////////////////////////modal editar/////////////////////////////////////////-->

<div class="modal fade" id="editarModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="titulo"><strong></strong></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <input type='hidden' id='numCaso' name='numCaso' class='form-control' readonly='readonly'/>
        <input type='hidden' id='numRadNumber' name='numRadNumber' class='form-control' readonly='readonly'/> 
          <div id="editarForm">
            <!-- aqui va el formulario -->


          </div

        <form  method="post" id="editaDoc" accept-charset="utf-8" enctype="multipart/form-data">
          <div class='form-group col-md-10'>
        <h4>3.- Seleccione los siguientes archivos desde su computadora.</h4>
        <table class='table table-hover'>
        <thead>
          <tr>  
            <th scope='col'>Documento</th>
            <th scope='col'>Anexo</th>
          </tr>
        </thead>
          <tbody>
            <tr>
              <th scope='row'><label>FORMATO DE SOLICITUD DEL DESARROLLADOR</label></th>
                <td><input type="file" id="edarchivo" name="edarchivo" class="form-control" onchange="edfichero.value=this.value.replace('C:\\fakepath\\', '');">
                <input type='hidden' id="edfichero" name="edfichero" class="form-control">
                <br>
                <input type="hidden" id="edbase64textarea" name="edbase64textarea" class='form-control' /></td>
            </tr>
            <tr>
              <th scope='row'><label>Copia de factibilidad autorizada y vigente</label></th>
                <td><input type='file' id='edarchivo2' name='edarchivo2' class='form-control' onchange="edfichero2.value=this.value.replace('C:\\fakepath\\', '');">
                <input type='hidden' id="edfichero2" name='edfichero2' class='form-control'>
                <br>
                <input type='hidden' id="edbase64textarea2" name='edbase64textarea2' class='form-control' value=''/></td>
            </tr>
          <tr>
            <th scope='row'><label>Archivo polígono KML (para todos los tramites ingresados antes de enero 2018)</label></th>
              <td colspan='2'><input type='file' id='edarchivo3' name='archivo3' class='form-control' onchange='edfichero3.value=this.value.replace("C:\\fakepath\\", "");'>
              <input type='hidden' id="edfichero3" name='edfichero3' class='form-control'>
              <br>
              <input type='hidden' id='edbase64textarea3' name='edbase64textarea3' class='form-control' /></td>
          </tr>
           </tbody>
         </table>   
    
      <div class='modal-footer'>
<div id='precarga'></div>
<input type='button' id='btn_enviar' name='creaCaso' value='Enviar Solicitud' class='btn btn-primary' onclick='realizaProcesoEditar()'/>
</div>
    </div>
  </div>
</div>
</form>
        

<!--modal editar-->

  <script>
     $('.popover-dismiss').popover({
  trigger: 'focus'
})
      </script>
      <script>
    //importamos configuraciones de toastr
        toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
          }
        </script>
      <script>
          var handleFileSelect = function(evt) {
            var files = evt.target.files;
            var file = files[0];
        
            if (files && file) {
                var reader = new FileReader();
        
                reader.onload = function(readerEvt) {
                    var binaryString = readerEvt.target.result;
                    document.getElementById("base64textarea").value = btoa(binaryString);
                };
        
                reader.readAsBinaryString(file);
            }
        };
        
        if (window.File && window.FileReader && window.FileList && window.Blob) {
            document.getElementById('filePicker').addEventListener('change', handleFileSelect, false);
        } else {
            alert('The File APIs are not fully supported in this browser.');
        }
        </script>   

<script>
    var handleFileSelect = function(evt) {
      var files = evt.target.files;
      var file = files[0];
  
      if (files && file) {
          var reader = new FileReader();
  
          reader.onload = function(readerEvt) {
              var binaryString = readerEvt.target.result;
              document.getElementById("base64textarea2").value = btoa(binaryString);
          };
          reader.readAsBinaryString(file);
      }
  };
  
  if (window.File && window.FileReader && window.FileList && window.Blob) {
      document.getElementById('filePicker2').addEventListener('change', handleFileSelect, false);
  } else {
      alert('The File APIs are not fully supported in this browser.');
  }
  </script>   

<script>
      var handleFileSelect = function(evt) {
      var files = evt.target.files;
      var file = files[0];
  
      if (files && file) {
          var reader = new FileReader();
  
          reader.onload = function(readerEvt) {
              var binaryString = readerEvt.target.result;
              document.getElementById("base64textarea3").value = btoa(binaryString);
          };
  
          reader.readAsBinaryString(file);
      }
  };
  
  if (window.File && window.FileReader && window.FileList && window.Blob) {
      document.getElementById('filePicker3').addEventListener('change', handleFileSelect, false);
  } else {
      alert('The File APIs are not fully supported in this browser.');
  }
  </script>  
  <script>
      var handleFileSelect = function(evt) {
        var files = evt.target.files;
        var file = files[0];    
        if (files && file) {
            var reader = new FileReader();
    
            reader.onload = function(readerEvt) {
                var binaryString = readerEvt.target.result;
                document.getElementById("base64textarea3").value = btoa(binaryString);
            };

            reader.readAsBinaryString(file);
        }
    };
    
    if (window.File && window.FileReader && window.FileList && window.Blob) {
        document.getElementById('filePicker3').addEventListener('change', handleFileSelect, false);
    } else {
        alert('The File APIs are not fully supported in this browser.');
    }
    </script>
<script>
function add()
{
  var elements = document.querySelectorAll(".entrada-usuario");
  var valor = document.getElementById('inEmail').value;

  for(var i = 0; i < elements.length;i++)
  {
     elements[i].value = valor;
  }
}
</script>

<!-- scripts para modal editar-->

 <script>
          var handleFileSelect = function(evt) {
            var files = evt.target.files;
            var file = files[0];
        
            if (files && file) {
                var reader = new FileReader();
        
                reader.onload = function(readerEvt) {
                    var binaryString = readerEvt.target.result;
                    document.getElementById("edbase64textarea").value = btoa(binaryString);
                };
        
                reader.readAsBinaryString(file);
            }
        };
        
        if (window.File && window.FileReader && window.FileList && window.Blob) {
            document.getElementById('edarchivo').addEventListener('change', handleFileSelect, false);
        } else {
            alert('The File APIs are not fully supported in this browser.');
        }
        </script>   

<script>
    var handleFileSelect = function(evt) {
      var files = evt.target.files;
      var file = files[0];
  
      if (files && file) {
          var reader = new FileReader();
  
          reader.onload = function(readerEvt) {
              var binaryString = readerEvt.target.result;
              document.getElementById("edbase64textarea2").value = btoa(binaryString);
          };
          reader.readAsBinaryString(file);
      }
  };
  
  if (window.File && window.FileReader && window.FileList && window.Blob) {
      document.getElementById('edarchivo2').addEventListener('change', handleFileSelect, false);
  } else {
      alert('The File APIs are not fully supported in this browser.');
  }
  </script>   

<script>
      var handleFileSelect = function(evt) {
      var files = evt.target.files;
      var file = files[0];
  
      if (files && file) {
          var reader = new FileReader();
  
          reader.onload = function(readerEvt) {
              var binaryString = readerEvt.target.result;
              document.getElementById("edbase64textarea3").value = btoa(binaryString);
          };
  
          reader.readAsBinaryString(file);
      }
  };
  
  if (window.File && window.FileReader && window.FileList && window.Blob) {
      document.getElementById('edarchivo3').addEventListener('change', handleFileSelect, false);
  } else {
      alert('The File APIs are not fully supported in this browser.');
  }
  </script>  
  <script>
      var handleFileSelect = function(evt) {
        var files = evt.target.files;
        var file = files[0];    
        if (files && file) {
            var reader = new FileReader();
    
            reader.onload = function(readerEvt) {
                var binaryString = readerEvt.target.result;
                document.getElementById("edbase64textarea3").value = btoa(binaryString);
            };

            reader.readAsBinaryString(file);
        }
    };
    
    if (window.File && window.FileReader && window.FileList && window.Blob) {
        document.getElementById('edarchivo3').addEventListener('change', handleFileSelect, false);
    } else {
        alert('The File APIs are not fully supported in this browser.');
    }
    </script>
<script>
  //Cuando la página esté cargada completamente
  $(document).ready(function(){
    //Cada 10 segundos se ejecutará la función refrescar
    setTimeout(refrescar, 10000);
  });
  function refrescar(){
    //Actualiza la el div con los datos de imagenes.php
    $("#tblCasos").load();
  }
</script>
</body>
</html>        