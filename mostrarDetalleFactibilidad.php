<?php 

error_reporting(E_ALL ^ E_NOTICE);
include 'conexion.php';
  $numfac = $_POST['factibilidades_slc'];
  
//obtener factibilidadhistorico
$sqlfh="select idA_Factibilidades from A_factibilidades fa inner join A_Factibilidades_EK fek on fa.No_Factibilidad=fek.No_Factibilidad where fa.No_Factibilidad='$numfac'";

$resultadofh = sqlsrv_query($conn,$sqlfh);
$values = sqlsrv_fetch_array($resultadofh);

$numHist = $values['idA_Factibilidades'];  

//obtener detalle de factibilidad
	
 $sqldf="select af.No_Factibilidad,af.Fecha_Vigencia, af.Desarrollo,af.Unidades,af.Domestico,af.Comercial,af.Industrial,af.Otros 
FROM A_Factibilidades af 
inner join A_Factibilidades_EK ak ON ak.No_Factibilidad=af.No_Factibilidad 
where ak.idA_Factibilidades = $numHist";
  $resultadodf = sqlsrv_query($conn,$sqldf);

  if (sqlsrv_has_rows($resultadodf)) {
    $tabla ="<br><label>Detalle Factibilidad:</label>";
    $tabla.="<table id='myTable' class='table table-bordered'>";
    $tabla.="<thead>";  
    $tabla.="<tr>";
    $tabla.="<th>Factibilidad</th>";
    $tabla.="<th>Fecha de Vigencia</th>";
    $tabla.="<th>Unidades</th>";
    $tabla.="<th>Domestico</th>";
    $tabla.="<th>Comercial</th>";
    $tabla.="<th>Industrial</th>";
    $tabla.="<th>Otros</th>";
    $tabla.="</tr>";
    $tabla.="</thead>";
    $tabla.="<tbody>";
      while ($row = sqlsrv_fetch_array($resultadodf)) {
        $tabla.= "<tr>";
          $tabla.= "<td>".$row["No_Factibilidad"]."</td>";
          if ($row["Fecha_Vigencia"] == "") {
            $tabla.= "<td>".$row["Fecha_Vigencia"]."</td>";
          }else{
            $tabla.= "<td>".$row["Fecha_Vigencia"]->format('d/m/Y')."</td>";
          }
          $tabla.= "<td>".$row["Unidades"]."</td>";
          if ($row["Domestico"] == "" || $row["Domestico"] == NULL) {
            $tabla.= "<td>0</td>";
          }else{
            $tabla.= "<td>".$row["Domestico"]."</td>";
          }  
          if ($row["Comercial"] == "" || $row["Comercial"] == NULL) {
            $tabla.= "<td>0</td>";
          }else{
            $tabla.= "<td>".$row["Comercial"]."</td>";
          }
          if ($row["Industial"] == "" || $row["Industial"] == NULL) {
            $tabla.= "<td>0</td>";
          }else{
            $tabla.= "<td>".$row["Industrial"]."</td>";
          }
          if ($row["Otros"] == "" || $row["Otros"] == NULL) {
            $tabla.= "<td>0</td>";
          }else{
            $tabla.= "<td>".$row["Otros"]."</td>";
          }
        $tabla.= "</tr>";
      
    }
    } 
    $tabla.="</tbody>";
    $tabla.="</table>";
    $tabla.= "<input type='hidden' id='numFacti' name='numFacti' class='form-control' value='$numHist'/>";

    $respuesta = $tabla;
  
   printf($respuesta);


 ?>
