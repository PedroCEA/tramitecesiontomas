<?php 
//error_reporting(E_ALL ^ E_NOTICE);
include 'conexion.php';

mssql_select_db("SCG",$conn);
//$newCaso=$_POST['newCaso'];
$rfc=$_POST['rfc'];
$tramite=$_POST['tramite'];
$correo=$_POST['correo'];
//$fecha=$_POST['fecha'];
$numExpediente=$_POST['n_expediente'];
$num_fac=$_POST['numFacti'];
$archivo=$_POST['fichero'];
$code1=$_POST['code1'];
$archivo2=$_POST['fichero2'];
$code2=$_POST['code2'];
$archivo3=$_POST['fichero3'];
$code3=$_POST['code3'];
$nombre=$_POST['nombre'];
$telefono=$_POST['telefono'];
$correoREP=$_POST['correoREP'];
$texto=$_POST['texto'];
$nume_facti=$_POST['factibilidades_slc'];

  // xml post structure

$soap_request = '<?xml version="1.0" encoding="UTF-8"?> <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/">
   <soapenv:Header/>
   <soapenv:Body>
      <tem:createCasesAsString>
         <!--Optional:-->
         <tem:casesInfo>
         <![CDATA[<BizAgiWSParam>
         <domain>cea</domain>
         <userName>'.$rfc.'</userName>
         <Cases>
               <Case>
                 <Process>TramitesFactibilidades</Process>
                 <Entities>
                  <Expediente>
                         <SolicitudPadron>
                          <Tipomovimiento>5</Tipomovimiento>
                            <Correo>'.$correo.'</Correo>
                         </SolicitudPadron>
                     </Expediente>
                 </Entities>
               </Case>
         </Cases>
         </BizAgiWSParam>]]>
         </tem:casesInfo>
      </tem:createCasesAsString>
   </soapenv:Body>
</soapenv:Envelope>';

    $headers = array(
         "Content-type: text/xml",
         "Accept: text/xml",
         "Cache-Control: no-cache",
         "Pragma: no-cache",
         "SOAPAction: http://tempuri.org/createCasesAsString",
         "Content-length: ".strlen($soap_request),
    );

    $url = "http://10.1.1.155/scg/WebServices/WorkflowEngineSOA.asmx";

    $ch = curl_init($url);
     curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
     curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
     curl_setopt($ch, CURLOPT_POST, 1);
     curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
     curl_setopt($ch, CURLOPT_POSTFIELDS, $soap_request);
     curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
     curl_setopt($ch, CURLOPT_VERBOSE, true);
     curl_setopt($ch, CURLOPT_TIMEOUT,10);
     
     $output = curl_exec($ch);

    curl_close($ch);


    $cadena=substr($output,443,13);
    $radNumber= $cadena;

  /*
    $consulta_save1 = "select * from WFCASE order by idCase desc";
    $sql_r1=sqlsrv_query($conn,$consulta_save1);
    $value1 = sqlsrv_fetch_array($sql_r1);
    //print_r($values1);
    $radNumber= $value1['radNumber'];
  */

   //===========================================Realizar Actividad======================================================================  

      $soap_request1 = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/">
      <soapenv:Header/>
      <soapenv:Body>
         <tem:performActivityAsString>
            <!--Optional:-->
            <tem:activityInfo> <![CDATA[<BizAgiWSParam>
                        <domain>cea</domain>
                        <userName>'.$rfc.'</userName>
                           <ActivityData>
                              <radNumber>'.$radNumber.'</radNumber>
                              <taskId>7146</taskId>
                           </ActivityData>
                           <Entities>
                           <Expediente>
                           <SolicitudPadron>

                              <Tipomovimiento>5</Tipomovimiento>
                              <Correo>'.$correo.'</Correo>
                           </SolicitudPadron>
                           <Noexpediente>'.$numExpediente.'</Noexpediente>
                           <FactibilidadHistorico>'.$num_fac.'</FactibilidadHistorico>                         
                              <Documentoss>    
                                     <Documentos>
                                         <Tipo>5519</Tipo>
                                         <Documento>
                                         <File fileName="'.$archivo.'">'.$code1.'</File></Documento>
                                     </Documentos>
                              <Documentos>
                                          <Tipo>121</Tipo>
                                         <Documento>
                                         <File fileName="'.$archivo3.'">'.$code3.'</File></Documento>
                                 </Documentos>
                                 <Documentos>
					<Tipo>15717</Tipo>
                                         <Documento>
                                         <File fileName="'.$archivo2.'">'.$code2.'</File></Documento>
                                        
                                 </Documentos>
                           </Documentoss>    
                           <Representantes>
                                 <Representantes>
                                    <Nombre>'.$nombre.'</Nombre>
                                    <Telefono>'.$telefono.'</Telefono>
                                    <Correo>'.$correoREP.'</Correo>                  
                                 </Representantes>                        
                           </Representantes> 
                           <Cedula>
                               <Cambioprorroga>'.$texto.'</Cambioprorroga>
                           </Cedula>
                       </Expediente>
                      </Entities>
                        </BizAgiWSParam>]]></tem:activityInfo>
         </tem:performActivityAsString>
      </soapenv:Body>
   </soapenv:Envelope>';

    
    $headers1 = array(
         "Content-type: text/xml",
         "Accept: text/xml",
         "Cache-Control: no-cache",
         "Pragma: no-cache",
         "SOAPAction: http://tempuri.org/performActivityAsString",
         "Content-length: ".strlen($soap_request1),
    );

    $url1 = "http://10.1.1.67/scg/WebServices/WorkflowEngineSOA.asmx";

    $ch1 = curl_init($url1);



     curl_setopt($ch1, CURLOPT_SSL_VERIFYHOST, 0);
     curl_setopt($ch1, CURLOPT_SSL_VERIFYPEER, 0);
     curl_setopt($ch1, CURLOPT_POST, 1);
     curl_setopt($ch1, CURLOPT_HTTPHEADER, $headers1);
     curl_setopt($ch1, CURLOPT_POSTFIELDS, $soap_request1);
     curl_setopt($ch1, CURLOPT_RETURNTRANSFER, 1);
     curl_setopt($ch1, CURLOPT_VERBOSE, true);
     curl_setopt($ch1, CURLOPT_TIMEOUT,10);

     $resultado = curl_exec($ch1);
     curl_close($ch1);

     //============================================Enviar a CEA===========================================

     $consulta_save1 = "select * from WFCASE order by idCase desc";
     $sql_r1=mssql_query($consulta_save1);
     $value1 = mssql_fetch_array($sql_r1);
     //print_r($values1);
     $radNumber= $value1['radNumber'];

     $fecha=date("Y-m-d H:i:s");

     $requiero='<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/">
         <soapenv:Header/>
         <soapenv:Body>
            <tem:performActivityAsString>
               <!--Optional:-->
               <tem:activityInfo> <![CDATA[<BizAgiWSParam>
                           <domain>cea</domain>
                           <userName>'.$rfc.'</userName>
                              <ActivityData>
                                 <radNumber>'.$radNumber.'</radNumber>
                                 <taskId>7147</taskId>
                              </ActivityData>
                              <Entities>
                              <Expediente>
                              <SolicitudPadron>
                                <Folio>'.$radNumber.'</Folio>
                                 <Tipomovimiento>5</Tipomovimiento>
                                 <Correo>'.$correo.'</Correo>
                                 <Fecha>'.$fecha.'</Fecha>
                              </SolicitudPadron>  
                              <FactibilidadHistorico>'.$num_fac.'</FactibilidadHistorico>
                               <No_Factibilidad>'.$nume_facti.'</No_Factibilidad>
                          </Expediente>
                         </Entities>
               </BizAgiWSParam>]]></tem:activityInfo>
            </tem:performActivityAsString>
         </soapenv:Body>
      </soapenv:Envelope>';

      $headers2 = array(

         "Content-type: text/xml",
         "Accept: text/xml",
         "Cache-Control: no-cache",
         "Pragma: no-cache",
         "SOAPAction: http://tempuri.org/performActivityAsString",
         "Content-length: ".strlen($requiero),
      );


    $url2 = "http://10.1.1.67/scg/WebServices/WorkflowEngineSOA.asmx";
    $ch2 = curl_init($url2);

     curl_setopt($ch2, CURLOPT_SSL_VERIFYHOST, 0);
     curl_setopt($ch2, CURLOPT_SSL_VERIFYPEER, 0);
     curl_setopt($ch2, CURLOPT_POST, 1);
     curl_setopt($ch2, CURLOPT_HTTPHEADER, $headers2);
     curl_setopt($ch2, CURLOPT_POSTFIELDS, $requiero);
     curl_setopt($ch2, CURLOPT_RETURNTRANSFER, 1);
     curl_setopt($ch2, CURLOPT_VERBOSE, true);
     curl_setopt($ch2, CURLOPT_TIMEOUT,10);

     $resultado2 = curl_exec($ch2);
    
     if ($resultado2) 
     {

      $respuesta =  "<div class ='alert alert-success' data-recargar>Se creó la solicitud con exito!!!!.</div>";

     }else{

      $respuesta = "<div class ='error'>Hubo un error. no se pudo insertar.</div>";

     }

     curl_close($ch2);
     
     //echo $requiero;
     echo $soap_request1;
 ?>