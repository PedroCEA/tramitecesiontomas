<?php 

require_once 'conexion.php';

if(isset($_POST['edit_id'])){
  $folio=$_POST['edit_id'];

$obCase="select * from wfcase where radNumber='$folio'";  

 $sql1=sqlsrv_query($conn,$obCase);
  $result = sqlsrv_fetch_array($sql1);
 

$caso=$result['idCase'];


$consulta="select ex.idExpediente,ex.Noexpediente,ex.Num_factibilidad,sp.Folio,sp.Usuario,sp.Tipotramite,sp.Tipomovimiento,sp.Correo,sp.Fecha,c.Superficie,c.Unidadescesion
from Expediente ex 
inner join SolicitudPadron sp on ex.SolicitudPadron=sp.idSolicitudPadron
inner join Cedula c on ex.Cedula=c.idCedula
where sp.Folio='$folio'";

$sql=sqlsrv_query($conn,$consulta);

  while ($row=sqlsrv_fetch_array($sql)) {
    $expediente=$row['idExpediente'];
    $folioed=$row['Folio'];
    $fechaed=$row['Fecha']->format('d-m-Y');
    $correoed=$row['Correo'];
    $noexpe=$row['Noexpediente'];
    $nume_facti=$row['Num_factibilidad'];
    $superf=$row['Superficie'];
    $unidades=$row['Unidadescesion'];
    $usuario=$row['Usuario'];
  }


$consulta="SELECT userName FROM WFUSER WHERE idUser LIKE '$usuario'";

  $sql=sqlsrv_query($conn,$consulta);
  $values = sqlsrv_fetch_array($sql);

$userrfc=$values['userName'];
  
}
?>

                            <h4>Coloca el correo </h4>
                             <form id='edita-caso' name='miformulario' method='POST' enctype='multipart/form-data'>
                                    <div class="row">
                                      <div class="col-md-4">
                                        <label for="folio">Folio:</label>
                                        <input type="text" id="folioed" name="folioed"  class="form-control" value="<?php echo $folio; ?>" readonly>
                                        <input type="hidden" id="rfced" name="rfced" value="<?php echo $userrfc; ?>">
                                      </div>
                                      <div class="col-md-8">
                                        <label for="tramite">Tramite:</label>
                                        <input type="text" id="tramiteed" name="tramiteed" class="form-control" value="Cesión de Tomas" readonly>
                                      </div>
                                    </div>
                                    <div class="row">
                                      <div class="col-md-3">
                                        <label for="fecha">Fecha:</label>
                                        <input type="text" id="fechaed" class="form-control" name="fechaed" value="<?php echo $fechaed; ?>" readonly>
                                      </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                      <div class="col-md-3">
                                        <label for="correo">Correo:</label>
                                        <input type="text" id="correoed" name="correoed" class="form-control" value="<?php echo $correoed ?>" readonly>
                                      </div>
                                    </div>
                                    <br>
                                    <h2>Colocar numero de expediente</h2>
                                    <div class="row">
                                      <div class="col-md-6">
                                       <label for="n_expediente">Nº Expediente:</label>
                                        <div class="input-group mb-3">
                                          <input type="text" id="n_expedienteed" name="n_expedienteed" class="form-control" aria-label="Recipient's username" aria-describedby="button-addon2" value="<?php echo $noexpe; ?>" readonly>
                                          <div class="input-group-append">
                                            <button class="btn btn-outline-secondary" type="button" id="button-addon2" disabled>Buscar</button>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="col-md-4">
                                        <label for="">No Factibilidad</label>
                                        <div id="select2lista">
                                          <input type="text" id="factibilidades_slced" name="factibilidades_slced" value="<?php echo $nume_facti; ?>" class="form-control" readonly />
                                        </div>
                                      </div>
                                      <div class="col-md-2">
                                        <a tabindex="0" class="btn popover-dismiss" role="button" data-toggle="popover" data-placement="bottom" data-trigger="focus" data-content="En caso de no encontrar su factibilidad favor de comuniarse con el area de sistemas de la CEA"><img src="img/mark_help.png" alt="" srcset=""></a>
                                      </div>   
                                    </div>
                                    
                                    <div class="col-md-10">
                                      <label>Detalle Factibilidad:</label>
                                        <div id='tabla'></div>
                                    </div>

                                    <div class="row">
                                      <div class="col-md-6">
                                        <label for="superficie">Superficie:</label>
                                        <input type="number" id="superficieed" name="superficieed" class="form-control" value="<?php  echo $superf?>" readonly>
                                      </div>
                                      <div class="col-md-6">
                                        <label for="">Cantidad total de viviendas a ceder:</label>
                                        <input type="number" id="totViviendased" name="totViviendased" class="form-control" value="<?php echo $unidades; ?>" readonly>
                                      </div>
                                    </div>
                                    <br>

                                    <div class="row">
                                      <div class="col-md-12">
                                        <h2>Agregar Documentos</h2>
                                        <table class="table">
                                          <thead>
                                            <tr>
                                              <th>Documento</th>
                                              <th>Anexo</th>  
                                            </tr>
                                          </thead>
                                          <tbody>
                                            <tr>
                                              <td>Solicitud para trámite de factibilidad de servicios.</td>
                                              <td><input type="file" name="file-1ed" id="file-1ed" class="form-control"  onchange="fichero1ed.value=this.value.replace('C:\\fakepath\\', '');"/>
                                              <input type="hidden" id="fichero1ed" name="fichero1ed" class="form-control">
                                              <input type='hidden' id='code1ed' name='code1ed' class='form-control' />
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>Copia del informe de uso de suelo.</td>
                                              <td><input type="file" name="file-2ed" id="file-2ed" class="form-control" onchange="fichero2ed.value=this.value.replace('C:\\fakepath\\', '');"/>
                                              <input type="hidden" id="fichero2ed" name="fichero2ed" class="form-control">
                                              <input type='hidden' id='code2ed' name='code2ed' class='form-control' />
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>Croquis del predio en Google Earth.</td>
                                              <td><input type="file" name="file-3ed" id="file-3ed" class="form-control" onchange="fichero3ed.value=this.value.replace('C:\\fakepath\\', '');"/>
                                              <input type="hidden" id="fichero3ed" name="fichero3ed" class="form-control">
                                              <input type='hidden' id='code3ed' name='code3ed' class='form-control' />
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>Croquis del predio en plan parcial del desarrollo.</td>
                                              <td><input type="file" name="file-4ed" id="file-4ed" class="form-control" onchange="fichero4ed.value=this.value.replace('C:\\fakepath\\', '');"/>
                                              <input type="hidden" id="fichero4ed" name="fichero4ed" class="form-control">
                                              <input type='hidden' id='code4ed' name='code4ed' class='form-control' />
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>Copia certificada de la escritura de propiedad del predio con datos del Registro Público de la Propiedad.</td>
                                              <td><input type="file" name="file-5ed" id="file-5ed" class="form-control" onchange="fichero5ed.value=this.value.replace('C:\\fakepath\\', '');"/>
                                              <input type="hidden" id="fichero5ed" name="fichero5ed" class="form-control">
                                              <input type='hidden' id='code5ed' name='code5ed' class='form-control' />
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>Copia certificada del Acta Constitutiva con datos en el Registro Público de la Propiedad.</td>
                                              <td><input type="file" name="file-6ed" id="file-6ed" class="form-control" onchange="fichero6ed.value=this.value.replace('C:\\fakepath\\', '');"/>
                                              <input type="hidden" id="fichero6ed" name="fichero6ed" class="form-control">
                                              <input type='hidden' id='code6ed' name='code6ed' class='form-control' />  
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>Copia certificada del poder notarial (representante legal).</td>
                                              <td><input type="file" name="file-7ed" id="file-7ed" class="form-control" onchange="fichero7ed.value=this.value.replace('C:\\fakepath\\', '');"/>
                                              <input type="hidden" id="fichero7ed" name="fichero7ed" class="form-control">
                                              <input type='hidden' id='code7ed' name='code7ed' class='form-control' />  
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>Copia de la inscripción en el RFC.</td>
                                              <td><input type="file" name="file-8ed" id="file-8ed" class="form-control" onchange="fichero8ed.value=this.value.replace('C:\\fakepath\\', '');"/>
                                              <input type="hidden" id="fichero8ed" name="fichero8ed" class="form-control">
                                              <input type='hidden' id='code8ed' name='code8ed' class='form-control' />  
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>Copia de la identificación del propietario del predio (en caso de tratarse de una persona física).</td>
                                              <td><input type="file" name="file-9ed" id="file-9ed" class="form-control" onchange="fichero9ed.value=this.value.replace('C:\\fakepath\\', '');"/>
                                              <input type="hidden" id="fichero9ed" name="fichero9ed" class="form-control">
                                              <input type='hidden' id='code9ed' name='code9ed' class='form-control' />  
                                              </td>
                                            </tr>
                                            <tr>
                                            <td>Copia de la identificación del representante legal-</td>
                                              <td><input type="file" name="file-10ed" id="file-10ed" class="form-control" onchange="fichero10ed.value=this.value.replace('C:\\fakepath\\', '');"/>
                                                <input type="hidden" id="fichero10ed" name="fichero10ed" class="form-control">
                                              <input type='hidden' id='code10ed' name='code10ed' class='form-control' />  
                                              </td>
                                            </tr>
                                            <tr>
                                            <td>Archivo digital con el predio georreferenciado en formato KML o KMZ,sistema de coordenadas WGS84.</td>
                                              <td><input type="file" name="file-11ed" id="file-11ed" class="form-control" onchange="fichero11ed.value=this.value.replace('C:\\fakepath\\', '');"/>
                                                <input type="hidden" id="fichero11ed" name="fichero11ed" class="form-control">
                                              <input type='hidden' id='code11ed' name='code11ed' class='form-control' />  
                                              </td>
                                            </tr>
                                            <tr>
                                            <td>Copia certificada de la cesión de tomas en escritura pública ante notario, especificando de que oficio inicial y cuantas tomas le ceden,así como se realizó el pago en su momento.</td>
                                              <td><input type="file" name="file-12ed" id="file-12ed" class="form-control" onchange="fichero12ed.value=this.value.replace('C:\\fakepath\\', '');"/>
                                              <input type="hidden" id="fichero12ed" name="fichero12ed" class="form-control">
                                              <input type='hidden' id='code12ed' name='code12ed' class='form-control' />  
                                              </td>
                                            </tr>
      
                                          </tbody>
                                        </table>
                                      </div>
                                    </div>
                                
                               
                                <div class="modal-footer">
                                 
                                  <button type="button" class="btn btn-primary" onclick="realizaProcesoEditar()"><i class="fas fa-check"></i> Enviar Solicitud</button>
                                
                                
                              </div>
                            </div>
                          </div>
                        </form>

                     
                    </div>



     <script>
    $('.popover-dismiss').popover({
      trigger: 'focus'
    })
  </script>                
    <!--////////////////////////////////////// scripts para base 64 ///////////////////////////////////////////////////////-->

    <script>
    var handleFileSelect = function(evt) {
    var files = evt.target.files;
    var file = files[0];

    if (files && file) {
      var reader = new FileReader();

    reader.onload = function(readerEvt) {
      var binaryString = readerEvt.target.result;
      document.getElementById("code1ed").value = btoa(binaryString);
    };

      reader.readAsBinaryString(file);
    }
    };

    if (window.File && window.FileReader && window.FileList && window.Blob) {
      document.getElementById('file-1ed').addEventListener('change', handleFileSelect, false);
    } else {
      alert('The File APIs are not fully supported in this browser.');
    }
  </script>

  <script>
    var handleFileSelect = function(evt) {
    var files = evt.target.files;
    var file = files[0];

    if (files && file) {
      var reader = new FileReader();

    reader.onload = function(readerEvt) {
      var binaryString = readerEvt.target.result;
      document.getElementById("code2ed").value = btoa(binaryString);
    };

      reader.readAsBinaryString(file);
    }
    };

    if (window.File && window.FileReader && window.FileList && window.Blob) {
      document.getElementById('file-2ed').addEventListener('change', handleFileSelect, false);
    } else {
      alert('The File APIs are not fully supported in this browser.');
    }
  </script>

  <script>
    var handleFileSelect = function(evt) {
    var files = evt.target.files;
    var file = files[0];

    if (files && file) {
      var reader = new FileReader();

    reader.onload = function(readerEvt) {
      var binaryString = readerEvt.target.result;
      document.getElementById("code3ed").value = btoa(binaryString);
    };

      reader.readAsBinaryString(file);
    }
    };

    if (window.File && window.FileReader && window.FileList && window.Blob) {
      document.getElementById('file-3ed').addEventListener('change', handleFileSelect, false);
    } else {
      alert('The File APIs are not fully supported in this browser.');
    }
  </script>

  <script>
    var handleFileSelect = function(evt) {
    var files = evt.target.files;
    var file = files[0];

    if (files && file) {
      var reader = new FileReader();

    reader.onload = function(readerEvt) {
      var binaryString = readerEvt.target.result;
      document.getElementById("code4ed").value = btoa(binaryString);
    };

      reader.readAsBinaryString(file);
    }
    };

    if (window.File && window.FileReader && window.FileList && window.Blob) {
      document.getElementById('file-4ed').addEventListener('change', handleFileSelect, false);
    } else {
      alert('The File APIs are not fully supported in this browser.');
    }
  </script>

  <script>
    var handleFileSelect = function(evt) {
    var files = evt.target.files;
    var file = files[0];

    if (files && file) {
      var reader = new FileReader();

    reader.onload = function(readerEvt) {
      var binaryString = readerEvt.target.result;
      document.getElementById("code5ed").value = btoa(binaryString);
    };

      reader.readAsBinaryString(file);
    }
    };

    if (window.File && window.FileReader && window.FileList && window.Blob) {
      document.getElementById('file-5ed').addEventListener('change', handleFileSelect, false);
    } else {
      alert('The File APIs are not fully supported in this browser.');
    }
  </script>

  <script>
    var handleFileSelect = function(evt) {
    var files = evt.target.files;
    var file = files[0];

    if (files && file) {
      var reader = new FileReader();

    reader.onload = function(readerEvt) {
      var binaryString = readerEvt.target.result;
      document.getElementById("code6ed").value = btoa(binaryString);
    };

      reader.readAsBinaryString(file);
    }
    };

    if (window.File && window.FileReader && window.FileList && window.Blob) {
      document.getElementById('file-6ed').addEventListener('change', handleFileSelect, false);
    } else {
      alert('The File APIs are not fully supported in this browser.');
    }
  </script>

  <script>
    var handleFileSelect = function(evt) {
    var files = evt.target.files;
    var file = files[0];

    if (files && file) {
      var reader = new FileReader();

    reader.onload = function(readerEvt) {
      var binaryString = readerEvt.target.result;
      document.getElementById("code7ed").value = btoa(binaryString);
    };

      reader.readAsBinaryString(file);
    }
    };

    if (window.File && window.FileReader && window.FileList && window.Blob) {
      document.getElementById('file-7ed').addEventListener('change', handleFileSelect, false);
    } else {
      alert('The File APIs are not fully supported in this browser.');
    }
  </script>

  <script>
    var handleFileSelect = function(evt) {
    var files = evt.target.files;
    var file = files[0];

    if (files && file) {
      var reader = new FileReader();

    reader.onload = function(readerEvt) {
      var binaryString = readerEvt.target.result;
      document.getElementById("code8ed").value = btoa(binaryString);
    };

      reader.readAsBinaryString(file);
    }
    };

    if (window.File && window.FileReader && window.FileList && window.Blob) {
      document.getElementById('file-8ed').addEventListener('change', handleFileSelect, false);
    } else {
      alert('The File APIs are not fully supported in this browser.');
    }
  </script>

  <script>
    var handleFileSelect = function(evt) {
    var files = evt.target.files;
    var file = files[0];

    if (files && file) {
      var reader = new FileReader();

    reader.onload = function(readerEvt) {
      var binaryString = readerEvt.target.result;
      document.getElementById("code9ed").value = btoa(binaryString);
    };

      reader.readAsBinaryString(file);
    }
    };

    if (window.File && window.FileReader && window.FileList && window.Blob) {
      document.getElementById('file-9ed').addEventListener('change', handleFileSelect, false);
    } else {
      alert('The File APIs are not fully supported in this browser.');
    }
  </script>

  <script>
    var handleFileSelect = function(evt) {
    var files = evt.target.files;
    var file = files[0];

    if (files && file) {
      var reader = new FileReader();

    reader.onload = function(readerEvt) {
      var binaryString = readerEvt.target.result;
      document.getElementById("code10ed").value = btoa(binaryString);
    };

      reader.readAsBinaryString(file);
    }
    };

    if (window.File && window.FileReader && window.FileList && window.Blob) {
      document.getElementById('file-10ed').addEventListener('change', handleFileSelect, false);
    } else {
      alert('The File APIs are not fully supported in this browser.');
    }
  </script>

  <script>
    var handleFileSelect = function(evt) {
    var files = evt.target.files;
    var file = files[0];

    if (files && file) {
      var reader = new FileReader();

    reader.onload = function(readerEvt) {
      var binaryString = readerEvt.target.result;
      document.getElementById("code11ed").value = btoa(binaryString);
    };

      reader.readAsBinaryString(file);
    }
    };

    if (window.File && window.FileReader && window.FileList && window.Blob) {
      document.getElementById('file-11ed').addEventListener('change', handleFileSelect, false);
    } else {
      alert('The File APIs are not fully supported in this browser.');
    }
  </script>

  <script>
    var handleFileSelect = function(evt) {
    var files = evt.target.files;
    var file = files[0];

    if (files && file) {
      var reader = new FileReader();

    reader.onload = function(readerEvt) {
      var binaryString = readerEvt.target.result;
      document.getElementById("code12ed").value = btoa(binaryString);
    };

      reader.readAsBinaryString(file);
    }
    };

    if (window.File && window.FileReader && window.FileList && window.Blob) {
      document.getElementById('file-12ed').addEventListener('change', handleFileSelect, false);
    } else {
      alert('The File APIs are not fully supported in this browser.');
    }
  </script>
    <!--////////////////////////////////////// scripts para base 64 ///////////////////////////////////////////////////////-->