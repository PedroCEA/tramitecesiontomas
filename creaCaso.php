<?php

    function multiexplode ($delimiters,$string) {
        $ready = str_replace($delimiters, $delimiters[0], $string);
        $launch = explode($delimiters[0], $ready);
        return  $launch;
    }

    $rfc=$_POST['rfc'];

    $soap_request = '<?xml version="1.0" encoding="UTF-8"?> <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/">
    <soapenv:Header/>
        <soapenv:Body>
        <tem:createCasesAsString>
        <!--Optional:-->
            <tem:casesInfo>
            <![CDATA[<BizAgiWSParam>
                <domain>CEA</domain>
                <userName>'.$rfc.'</userName>
                    <Cases>
                    <Case>
                    <Process>CopySolicitudFactibilidadD</Process>
                        <Entities>
                        <Expediente>
                        </Expediente>
                        </Entities>
                    </Case>
                    </Cases>
            </BizAgiWSParam>]]>
            </tem:casesInfo>
        </tem:createCasesAsString>
        </soapenv:Body>
    </soapenv:Envelope>';

    $headers = array(
    "Content-type: text/xml",
    "Accept: text/xml",
    "Cache-Control: no-cache",
    "Pragma: no-cache",
    "SOAPAction: http://tempuri.org/createCasesAsString",
    "Content-length: ".strlen($soap_request),
    );

    $url = "http://10.1.1.155/SCG/WebServices/WorkflowEngineSOA.asmx";
    //$url = "http://10.1.1.67/SCG/WebServices/WorkflowEngineSOA.asmx";

    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $soap_request);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_VERBOSE, true);
    curl_setopt($ch, CURLOPT_TIMEOUT,10);
     
    $output = curl_exec($ch);
    curl_close($ch);
    $datos = multiexplode(array("&lt;","/","&gt;"), $output);
    $radNumber=$datos['29'];
    echo $radNumber;

?>    

